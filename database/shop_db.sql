-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2017 at 08:51 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `admin` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `admin`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `order_id` int(100) NOT NULL,
  `order_total` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `country` varchar(255) NOT NULL,
  `pcode` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billing`
--

INSERT INTO `billing` (`id`, `order_id`, `order_total`, `firstname`, `lastname`, `email`, `mobile`, `company`, `address`, `country`, `pcode`, `city`, `notes`) VALUES
(2, 18, '267', 'usman', 'yousaf', 'uzzy_chaudry@live.co.uk', '07421656446', 'webinventors', 'Address*', '', '55d55d', 'sheffeidl', 'Special Notes');

-- --------------------------------------------------------

--
-- Table structure for table `blog_comments`
--

CREATE TABLE `blog_comments` (
  `id` int(11) NOT NULL,
  `b_id` int(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date` date NOT NULL,
  `day` varchar(255) NOT NULL,
  `comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blog_comments`
--

INSERT INTO `blog_comments` (`id`, `b_id`, `name`, `email`, `time`, `date`, `day`, `comment`) VALUES
(3, 2, 'usman', 'ozzy.chaudry@gmail.com', '0000-00-00 00:00:00', '0000-00-00', 'Saturday', 'hello usman'),
(4, 2, 'usman', 'ozzy.chaudry@gmail.com', '0000-00-00 00:00:00', '0000-00-00', 'Tuesday', 'this is the test comment ');

-- --------------------------------------------------------

--
-- Table structure for table `emails`
--

CREATE TABLE `emails` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `emails`
--

INSERT INTO `emails` (`id`, `email`, `subject`, `message`, `date`) VALUES
(28, 'ozzy.chaudry@gmail.com', 'hello', '<p>hello usman how are you doing!</p>', '2017-03-11 20:42:45'),
(29, 'ozzy.chaudry@gmail.com', 'hello', '<p>hhhhhhhhhhhhhhhhaaaaaaaaaaay</p>', '2017-03-11 20:49:24'),
(30, 'ozzy.chaudry@gmail.com', 'hello', '<p>hello</p>', '2017-03-11 20:54:21'),
(31, 'ozzy.chaudry@gmail.com', 'hello', '<p>your</p>', '2017-03-11 21:24:01'),
(32, 'ozzy.chaudry@gmail.com', 'hello', '<p>helloooooooooooooooooooooooo</p>', '2017-03-11 21:27:17'),
(33, 'ozzy.chaudry@gmail.com', 'hello', '<p>hello usman</p>', '0000-00-00 00:00:00'),
(34, 'ozzy.chaudry@gmail.com', 'hello', '<p>hello usman</p>', '0000-00-00 00:00:00'),
(35, 'ozzy.chaudry@gmail.com', 'hello', '<p>helloooo</p>', '0000-00-00 00:00:00'),
(36, 'ozzy.chaudry@gmail.com', 'hello', '<p>hello Mr usman</p>', '0000-00-00 00:00:00'),
(38, 'ozzy.chaudry@gmail.com', 'hello', '<p>hello usman</p>', '0000-00-00 00:00:00'),
(39, 'uzzy_chaudry@live.co.uk', 'hello', '<p>hello there</p>', '0000-00-00 00:00:00'),
(40, 'ozzy.chaudry@gmail.com', 'hello', '<p>whats up</p>', '0000-00-00 00:00:00'),
(41, 'uzzy@gmail.com', 'hello', '<p>hello hheeee</p>', '0000-00-00 00:00:00'),
(42, 'ozzy.chaudry@gmail.com', 'Test Shop Email Marketing', '', '0000-00-00 00:00:00'),
(43, 'ozzy.chaudry@gmail.com', 'Test Shop Email Marketing', '', '0000-00-00 00:00:00'),
(44, 'ozzy.chaudry@gmail.com', 'Test Shop Email Marketing', '<p>hello</p>', '0000-00-00 00:00:00'),
(45, 'ozzy.chaudry@gmail.com', 'Test Shop Email Marketing', '<p>hello</p>', '0000-00-00 00:00:00'),
(46, 'ozzy.chaudry@gmail.com', 'Test Shop Email Marketing', '<p>hello sir how are you doing</p>', '0000-00-00 00:00:00'),
(47, 'ozzy.chaudry@gmail.com', 'Test Shop Email Marketing', '<p>test email</p>\r\n<p>&nbsp;</p>', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `email_sub`
--

CREATE TABLE `email_sub` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_sub`
--

INSERT INTO `email_sub` (`id`, `email`, `date`) VALUES
(1, 'uzzy_chaudry@live.co.uk', '0000-00-00 00:00:00'),
(2, 'ozzy.chaudry@gmail.com', '0000-00-00 00:00:00'),
(3, 'uzzy_chaudry@live.co.uk', '0000-00-00 00:00:00'),
(4, 'admin@admin.com', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `fbusers`
--

CREATE TABLE `fbusers` (
  `id` int(11) NOT NULL,
  `oauth_provider` enum('','facebook','google','twitter') NOT NULL,
  `oauth_uid` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `locale` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fbusers`
--

INSERT INTO `fbusers` (`id`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `picture`, `link`, `created`, `modified`) VALUES
(1, 'facebook', '1562527197110615', 'Ozzy', 'ChaudRy', 'uzzy_chaudry@live.co.uk', 'male', 'en_GB', 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/13432377_1291884604174877_7485040866886155392_n.jpg?oh=7973ae4a8d0c56ef6fa7c09aa219bcaa&oe=592B6153', 'https://www.facebook.com/app_scoped_user_id/1562527197110615/', '2017-03-17 04:42:53', '2017-03-18 01:58:46');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `company` varchar(255) NOT NULL,
  `message` varchar(300) NOT NULL,
  `date` date NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `day` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `subject`, `company`, `message`, `date`, `time`, `day`) VALUES
(1, 'usman', 'uzzy_chaudry@live.co.uk', 'testing', '', 'testing for the website ', '2001-11-16', '0000-00-00 00:00:00', 'Monday'),
(6, 'ozzy', 'ozzy.chaudry@gmail.com', 'hello', 'web', 'hello', '2003-04-17', '0000-00-00 00:00:00', 'Saturday'),
(8, 'usman ', 'uzzy_chaudry@live.co.uk', 'Send to All Users', 'webinventors', 'this is the test message ', '0000-00-00', '0000-00-00 00:00:00', 'Thursday'),
(9, 'usman', 'ozzy.chaudry@gmail.com', 'hello', 'webinventors', 'hello usman how are you doing', '0000-00-00', '0000-00-00 00:00:00', 'Saturday');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `quantity` varchar(100) NOT NULL,
  `r_points` int(100) NOT NULL,
  `discount` int(100) NOT NULL,
  `discount_p` int(100) NOT NULL,
  `order_total` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `username`, `quantity`, `r_points`, `discount`, `discount_p`, `order_total`) VALUES
(18, 'ozzy', '3', 30, 10, 24, 213),
(20, 'usman', '3', 30, 10, 28, 252);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `pagename` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `des` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `pagename`, `img`, `title`, `des`, `content`) VALUES
(1, 'about', '', 'aboutus', 'Our test test test shop and gallery with a really contemporary feel and layout to please the most discerning ', '<p><strong>Our test </strong><strong><strong>test </strong></strong><strong><strong><strong>test </strong></strong>shop and gallery with a really contemporary feel and layout to please the most discerning shopper offers a wide range of products from cuddly toys to fair trade gifts and locally sourced confectionery, all of which will remind you of your day at the zoo. &nbsp;Take a break in Coffee Corner where you can treat yourself with tempting sweet and savoury pastries, great cakes, baguettes and wraps.&nbsp;</strong></p>\r\n<p><img src="http://www.beeskneesonline.co.uk/images/needagift_banner.jpg" alt="" /></p>\r\n<p><strong>Gifts</strong></p>\r\n<p><strong>Looking for that perfect gift ? &nbsp;We have some amazing products to choose from! For the person who has everything (or thinks they do) don''t forget to visit the gallery or maybe a sculpture will make the perfect gift.</strong></p>\r\n<p><strong>Art Gallery</strong></p>\r\n<p><strong>Our Art Gallery displays a large and constantly updated selection of Artist-in-residence Pip McGarry''s original paintings, all available for sale.</strong></p>\r\n<p><strong>In our display cabinets are a collection of beautiful fossils, gems and wildlife related sculptures.&nbsp;</strong></p>\r\n<p><strong>Pip will be visiting the gallery from Saturday 5th &ndash; Sunday 6th December and again on Sunday 13th December.</strong></p>\r\n<p><strong>Pip will be sketching and chatting with our guests about the amazing paintings and sketches on display and sale in the Gallery.</strong></p>\r\n<p><strong>A Pip McGarry original makes the ultimate, exclusive Christmas gift.</strong></p>\r\n<p><strong>Cuddly toys</strong></p>\r\n<p><strong>Cute and cuddly - just like many of our animals. Choose from cheetah, tiger, penguin, snow leopard, giraffe, ring tailed lemur and red panda. Please contact us by email or call 01962 777984 to order. A delivery charge of &pound;3.50 per item applies.</strong></p>\r\n<p><strong>Mail order and general enquiries</strong></p>\r\n<p><strong>We operate a mail order service. Please contact us with your order on Monday, Wednesday or Friday from 10am - 3pm and we will package it and mail it to you. A delivery charge will apply. For Mail Order enquires please email shop@marwell.org.uk or call 01962 777984</strong></p>\r\n<p><strong>For general shop enquiries please call 01962 777976</strong></p>'),
(2, 'blog', '', 'blog', 'Who doesn''t love a gift? Being honest, we love presents on our birthdays', '<div id="accordion" class="panel-group">\r\n<div class="panel panel-default">\r\n<div id="headingOne" class="panel-heading">\r\n<h4 class="panel-title">&nbsp;</h4>\r\n<p style="margin: 1.5em 0px; color: #4c4c4c; font-family: open-sans, Helvetica, Helvetica; font-size: 17px; font-weight: 300; line-height: 27px;"><img class="alignright  wp-image-2050" style="vertical-align: middle;" title="gift" src="https://www.swipely.com/images/uploads/blog/gift.jpg" alt="" width="217" height="331" />why Ozzy doesn''t love a gift? Being honest, we love presents on our birthdays,&nbsp;<span id="cg_intext_rt_search_8" class="cg-intext-span cg-intext-span-reset cgspnlink " style="margin: 0px !important; padding: 0px !important; font-style: inherit !important; font-variant: inherit !important; font-weight: inherit !important; font-stretch: inherit !important; font-size: inherit !important; font-family: inherit !important; line-height: inherit !important; vertical-align: baseline !important; border: 0px !important; outline: 0px !important; width: auto !important; position: relative !important; background: none !important;" data-type="yahoo" data-type-id="10">loot around</span>&nbsp;the holidays, a wrapped box on those special occasions. It''s simple: everyone loves receiving presents.</p>\r\n<h4 class="panel-title">&nbsp;</h4>\r\n<p style="margin: 1.5em 0px; color: #4c4c4c; font-family: open-sans, Helvetica, Helvetica; font-size: 17px; font-weight: 300; line-height: 27px;">The other side to this coin is the shopping, and many people enjoy the challenge of choosing just the right gift to give someone else.</p>\r\n<h4 class="panel-title">&nbsp;</h4>\r\n<p style="margin: 1.5em 0px; color: #4c4c4c; font-family: open-sans, Helvetica, Helvetica; font-size: 17px; font-weight: 300; line-height: 27px;">For those in the&nbsp;<span id="cg_intext_rt_search_12" class="cg-intext-span cg-intext-span-reset cgspnlink " style="margin: 0px !important; padding: 0px !important; font-style: inherit !important; font-variant: inherit !important; font-weight: inherit !important; font-stretch: inherit !important; font-size: inherit !important; font-family: inherit !important; line-height: inherit !important; vertical-align: baseline !important; border: 0px !important; outline: 0px !important; width: auto !important; position: relative !important; background: none !important;" data-type="yahoo" data-type-id="10"><a id="cg_intext_rt_search_12_link" class="cg-intext-link-replace cgspnlink" style="float: none !important; text-decoration: underline !important; border-bottom-style: none !important; outline: 0px !important; position: relative !important; background: none transparent !important;" data-type="yahoo" data-type-id="10"></a>gift shop</span>&nbsp;business, blogging is a great way to connect with customers searching for that little perfect gift for that special someone.</p>\r\n<h4 class="panel-title">&nbsp;</h4>\r\n<p style="margin: 1.5em 0px; color: #4c4c4c; font-family: open-sans, Helvetica, Helvetica; font-size: 17px; font-weight: 300; line-height: 27px;"><span style="color: #333940;"><a style="color: #2995e2; text-decoration: none; transition: all 0.4s ease;" title="http://blog.flagladygifts.com/flags" href="http://blog.flagladygifts.com/flags" target="_blank">The Flag Lady&nbsp;<span id="cg_intext_rt_search_14" class="cg-intext-span cg-intext-span-reset cgspnlink  cg-intext-link-replace" style="margin: 0px !important; padding: 0px !important; font-style: inherit !important; font-variant: inherit !important; font-weight: inherit !important; font-stretch: inherit !important; font-size: inherit !important; font-family: inherit !important; line-height: inherit !important; vertical-align: baseline !important; border: 0px !important; outline: 0px !important; width: auto !important; position: relative !important; background: none !important;" data-type="yahoo" data-type-id="10">Gift Shop</span></a>: Knows Their Niche Market</span></p>\r\n<h4 class="panel-title">&nbsp;</h4>\r\n<p style="margin: 1.5em 0px; color: #4c4c4c; font-family: open-sans, Helvetica, Helvetica; font-size: 17px; font-weight: 300; line-height: 27px;"><a style="color: #2995e2; text-decoration: none; transition: all 0.4s ease;" title="http://blog.flagladygifts.com/flags" href="http://blog.flagladygifts.com/flags" target="_blank"><img class=" wp-image-1862 alignright" style="vertical-align: middle;" src="https://www.swipely.com/images/uploads/blog/logo.png" alt="" /></a></p>\r\n<h4 class="panel-title">&nbsp;</h4>\r\n<p style="margin: 1.5em 0px; color: #4c4c4c; font-family: open-sans, Helvetica, Helvetica; font-size: 17px; font-weight: 300; line-height: 27px;">The Flag Lady&nbsp;<span id="cg_intext_rt_search_16" class="cg-intext-span cg-intext-span-reset cgspnlink " style="margin: 0px !important; padding: 0px !important; font-style: inherit !important; font-variant: inherit !important; font-weight: inherit !important; font-stretch: inherit !important; font-size: inherit !important; font-family: inherit !important; line-height: inherit !important; vertical-align: baseline !important; border: 0px !important; outline: 0px !important; width: auto !important; position: relative !important; background: none !important;" data-type="yahoo" data-type-id="10"><a id="cg_intext_rt_search_16_link" class="cg-intext-link-replace cgspnlink" style="float: none !important; text-decoration: underline !important; border-bottom-style: none !important; outline: 0px !important; position: relative !important; background: none transparent !important;" data-type="yahoo" data-type-id="10"></a>Gift Shop is</span>&nbsp;located near Valley Forge, perfect for a shop specializing in selling flags. They use their blog to educate readers on different flags, posting stories on the history of different flags used in the area.</p>\r\n<h4 class="panel-title">&nbsp;</h4>\r\n<p style="margin: 1.5em 0px; color: #4c4c4c; font-family: open-sans, Helvetica, Helvetica; font-size: 17px; font-weight: 300; line-height: 27px;">Often using guest bloggers to keep things interesting,&nbsp;<span style="color: #333940;">Flag Lady Gifts</span>&nbsp;has posted articles&nbsp;about the history of the British flag, which almost sold out during the marriage of Prince William and Kate Middleton, and a fascinating post on the backwards United States flag used on military uniforms. It is clear that this&nbsp;<span id="cg_intext_rt_search_18" class="cg-intext-span cg-intext-span-reset cgspnlink " style="margin: 0px !important; padding: 0px !important; font-style: inherit !important; font-variant: inherit !important; font-weight: inherit !important; font-stretch: inherit !important; font-size: inherit !important; font-family: inherit !important; line-height: inherit !important; vertical-align: baseline !important; border: 0px !important; outline: 0px !important; width: auto !important; position: relative !important; background: none !important;" data-type="yahoo" data-type-id="10"><a id="cg_intext_rt_search_18_link" class="cg-intext-link-replace cgspnlink" style="float: none !important; text-decoration: underline !important; border-bottom-style: none !important; outline: 0px !important; position: relative !important; background: none transparent !important;" data-type="yahoo" data-type-id="10"></a>gift shop</span>&nbsp;knows about flags and that they want to share their knowledge with their readers and&nbsp;connect them with other great resources found on the internet.</p>\r\n<h4 class="panel-title">&nbsp;</h4>\r\n</div>\r\n</div>\r\n</div>'),
(6, '', '', 'Online Rewards Programs', 'The Importance of Social Media in Online Rewards Programs', '<h1 class="w-blog-title entry-title" style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: 300; font-stretch: normal; font-size: 50px; line-height: 1.4; font-family: Lato; color: #c82254;">The Importance of Social Media in Online Rewards Programs</h1>\r\n<p><img src="https://www.sweettoothrewards.com/wp-content/uploads/benefits-of-social-header-image.png" alt="benefits of social media in rewards programs" /></p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #3e3e3e; font-family: Lato; font-size: 16px;">How big of an impact does social media make on establishing customer loyalty? The short answer is a lot! The long answer is the content of this article.</p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #3e3e3e; font-family: Lato; font-size: 16px;">Social media has changed: the way marketers reach an audience, the level of communication between business and customer, and the way we share information. The social media wave has no doubt disrupted everything in business and rewards programs are no different.</p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #3e3e3e; font-family: Lato; font-size: 16px;">Here are 4 ways social media has made rewards programs better!</p>\r\n<h2 style="box-sizing: border-box; margin: 0px 0px 20px; padding: 10px 0px 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: 300; font-stretch: normal; font-size: 40px; line-height: 1.4; font-family: Lato; color: #c82254; text-align: center;">Social Media + Online Rewards Programs</h2>\r\n<p style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #3e3e3e; font-family: Lato; font-size: 16px;">Social media is a huge driver of customer loyalty and online rewards programs are well suited to take advantage. When your program is run online you have a much easier time capitalizing on social media as well as a&nbsp;<a style="box-sizing: border-box; text-decoration-line: none; transition: opacity 0.3s, padding 0.3s, color 0.3s, background-color 0.3s, box-shadow 0.3s, border 0.3s; color: #c82254; -webkit-font-smoothing: antialiased; text-shadow: rgba(0, 0, 0, 0.00392157) 1px 1px 1px;" href="https://www.sweettoothrewards.com/blog/benefits-online-loyalty-programs/">few other benefits</a>.</p>\r\n<h5 style="box-sizing: border-box; margin: 0px 0px 20px; padding: 10px 0px 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: 300; font-stretch: normal; font-size: 24px; line-height: 1.6; font-family: Lato; color: #c82254;">&nbsp;</h5>\r\n<h5 style="box-sizing: border-box; margin: 0px 0px 20px; padding: 10px 0px 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-weight: 300; font-stretch: normal; font-size: 24px; line-height: 1.6; font-family: Lato; color: #c82254;"><strong style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background: transparent;">1. Social media makes it easy to promote your program</strong></h5>\r\n<p style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #3e3e3e; font-family: Lato; font-size: 16px;">There is no easier way to reach a huge audience than with social media. A few clicks and you have access to 1.4 billion Facebook users, 300 million Twitter users, and a variety of other networks. Savvy marketers can use this to a huge advantage with their rewards programs.</p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #3e3e3e; font-family: Lato; font-size: 16px;"><img class="aligncenter size-full wp-image-8354" style="box-sizing: border-box; margin: 0px auto; padding: 0px; outline: 0px; vertical-align: baseline; background: transparent; max-width: 100%; height: auto; clear: both; display: block;" src="https://www.sweettoothrewards.com/wp-content/uploads/social-networks-count.png" alt="social networks for rewards programs" width="600" height="150" /></p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #3e3e3e; font-family: Lato; font-size: 16px;">This large audience makes social media the ideal place to promote your rewards program. You can let your entire Twitter following know that you have launched a rewards program. Or you could tell all your Facebook fans that you are now rewarding points for customer referrals.</p>\r\n<p style="box-sizing: border-box; margin: 0px 0px 20px; padding: 0px; border: 0px; outline: 0px; vertical-align: baseline; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; color: #3e3e3e; font-family: Lato; font-size: 16px;">Not only is it a great way to let existing customers know about your program but you can also use it to let people discover your amazing rewards program and brand. Just use hashtags that resonate with your potential customers. Do not miss this easy way to&nbsp;<a style="box-sizing: border-box; text-decoration-line: none; transition: opacity 0.3s, padding 0.3s, color 0.3s, background-color 0.3s, box-shadow 0.3s, border 0.3s; color: #c82254; -webkit-font-smoothing: antialiased; text-shadow: rgba(0, 0, 0, 0.00392157) 1px 1px 1px;" href="https://www.sweettoothrewards.com/blog/promoting-a-loyalty-program/">promote your program!</a></p>');

-- --------------------------------------------------------

--
-- Table structure for table `pageview`
--

CREATE TABLE `pageview` (
  `id` int(11) NOT NULL,
  `counts` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pageview`
--

INSERT INTO `pageview` (`id`, `counts`) VALUES
(1, '279');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `cateid` int(100) NOT NULL,
  `category` varchar(100) NOT NULL,
  `quantity` int(100) NOT NULL,
  `price` int(100) NOT NULL,
  `points` int(100) NOT NULL,
  `availability` varchar(100) NOT NULL,
  `des` varchar(200) NOT NULL,
  `img` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `cateid`, `category`, `quantity`, `price`, `points`, `availability`, `des`, `img`) VALUES
(36, 'Quiz Navy midi dress', 0, 'Women', 10, 24, 10, 'In stock', 'This dress is sure to get you noticed at the Christmas party. With a beautiful bardot style shoulder, bow feature and midi style, wear with heels and matching clutch for a stunning party look.', '61252_269722.jpg'),
(37, 'high stiletto court shoes', 0, 'Women', 10, 13, 10, 'In stock', 'Add a pop of colour to your outfit with these gorgeous turquoise stiletto court shoes from our fabulous range by Faith. They come in a sleek patent finish with a pointed toe and have a cushioned leath', '053010416941.jpg'),
(38, 'Black regular fit dinner suit', 0, 'men', 10, 160, 10, 'In stock', '2 button fastening, single breasted Satin notch lapel 4 satin covered cuff buttons Left and right straight side pockets plus breast pocket Side vents', '46497_0039629_290.jpg'),
(39, 'Pooh & piglet', 0, 'Kids', 10, 27, 10, 'In stock', 'When your babys ready to start exploring on hands and knees, bright and color full, you will love the look of these perennial cartoon ', '171050734699.jpg'),
(40, 'Calvin Klein CK Toilette 50ml', 0, 'sports', 10, 29, 10, 'In stock', 'A light, refreshing fragrance with an independent attitude. An intense combination of sensuality and freedom. To be shared by everyone. Calvin klein ck be. Use freely and often.', '20071130_123577902099.jpg'),
(41, 'Brown leather boots', 0, 'men', 10, 60, 10, 'In stock', 'These boots from Lotus are a timeless staple for the modern mans smart-casual line-up. Great for mixing with denim or chinos, they are finished in premium leather and offer lace up fastenings.', '084011557673.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `p_reviews`
--

CREATE TABLE `p_reviews` (
  `id` int(11) NOT NULL,
  `p_id` int(100) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(255) NOT NULL,
  `time` varchar(250) NOT NULL,
  `date` date NOT NULL,
  `day` varchar(250) NOT NULL,
  `review` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_reviews`
--

INSERT INTO `p_reviews` (`id`, `p_id`, `name`, `email`, `time`, `date`, `day`, `review`) VALUES
(6, 16, 'usman', 'uzzy_chaudry@live.co.uk', '19:59:34', '2001-09-16', 'Saturday', 'congratulations on being the only video in the entire internet on joins that is in englishï»¿congratulations on being the only video in the entire internet on joins that is in englishï»¿congratulations on being the only video in the entire internet on joins that is in englishï»¿congratulations on being the only video in the entire internet on joins that is in englishï»¿'),
(7, 13, 'usman', 'uzzy_chaudry@live.co.uk', '23:55:19', '2001-09-16', 'Saturday', 'congratulations on being the only video in the entire internet on joins that is in englishï»¿congratulations on being the only video in the entire internet on joins that is in englishï»¿congratulations on being the only video in the entire internet on joins that is in englishï»¿congratulations on being the only video in the entire internet on joins that is in englishï»¿'),
(8, 33, 'usman', 'uzzy_chaudry@live.co.uk', '19:55:24', '2001-10-16', 'Sunday', 'hey hohwho hgo gg ougghiwugdogeoyfgdouygoewgwueguycguygwogvwgovcgo'),
(12, 40, 'usman', 'uzzy_chaudry@live.co.uk', '14:13:35', '2001-11-16', 'Monday', 'A light, refreshing fragrance with an independent attitude. An intense combination of sensuality and freedom. To be shared by everyone. Calvin klein ck be. Use freely and often.'),
(16, 42, 'usman', 'ozzy.chaudry@gmail.com', '14:14:51', '2001-11-16', 'Monday', 'HP 250 Core i5-5200U 2.2GHz 4GB 500GB DVD-SM 15.6 Inch Windows 10 Home Laptop'),
(20, 38, 'sara', 'usman@gmail.com', '03-12-17', '0000-00-00', 'Sunday', 'i like this dress very much'),
(21, 0, 'usman', 'uzzy_chaudry@live.co.uk', '03-13-17', '0000-00-00', 'Monday', ''),
(22, 0, 'usman', 'uzzy_chaudry@live.co.uk', '03-13-17', '2001-11-03', 'Monday', ''),
(23, 38, 'usman', 'uzzy_chaudry@live.co.uk', '03-13-17', '0000-00-00', 'Monday', 'hello this is itesting'),
(25, 34, 'usman', 'uzzy_chaudry@live.co.uk', '03-15-17', '0000-00-00', 'Wednesday', 'heloo '),
(26, 41, 'usman', 'uzzy_chaudry@live.co.uk', '04-22-17', '0000-00-00', 'Saturday', 'hello ');

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE `seo` (
  `id` int(11) NOT NULL,
  `keywords` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `keywords`, `description`) VALUES
(1, '<p>amazing gifts, gifts, men clothes, women clothes, men shoes, women shoes, men Accessories, women Accessories, T-shirt, heels, kids clothes, kids shoes, kids T-shirt, bags, iphone, samsang galaxy, iphone6, iphone5, iphone 6 plus, samsung tablets, laptops, computers, desktop computers, sheffield , london, bradford, United Kingdom, health, beauty, watches, bracelets, rings, hand bags, glasses, ray ban,</p>', '<p>Our Test shop and gallery with a really contemporary feel and layout to please the most discerning shopper offers a wide range of products from cuddly toys to fair trade gifts and locally sourced confectionery, all of which will remind you of your day at the zoo. Take a break in Coffee Corner where you can treat yourself with tempting sweet and savoury pastries, great cakes, baguettes and wraps.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `google` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `des` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `total_shares`
--

CREATE TABLE `total_shares` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `shares` varchar(255) NOT NULL,
  `points` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `total_shares`
--

INSERT INTO `total_shares` (`id`, `user`, `shares`, `points`) VALUES
(6, 'ozzy', '1', '10'),
(7, 'ozzy', '1', '10'),
(8, 'ozzy', '1', '10'),
(9, 'ozzy', '1', '10'),
(10, 'ozzy', '1', '10'),
(11, 'ozzy', '1', '10'),
(12, 'ozzy', '1', '10'),
(13, 'usman', '1', '10'),
(14, 'ozzy', '1', '10'),
(15, 'ozzy', '1', '10'),
(16, 'ozzy', '1', '10'),
(17, 'ozzy', '1', '10'),
(18, 'ozzy', '1', '10');

-- --------------------------------------------------------

--
-- Table structure for table `usercart`
--

CREATE TABLE `usercart` (
  `id` int(11) NOT NULL,
  `pid` int(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `ptitle` varchar(100) NOT NULL,
  `price` int(100) NOT NULL,
  `points` int(100) NOT NULL,
  `quantity` int(100) NOT NULL,
  `totalcart` int(100) NOT NULL,
  `img` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usercart`
--

INSERT INTO `usercart` (`id`, `pid`, `username`, `ptitle`, `price`, `points`, `quantity`, `totalcart`, `img`) VALUES
(58, 38, 'ozzy', 'Black regular fit dinner suit', 160, 10, 1, 160, '46497_0039629_290.jpg'),
(77, 41, '<br/>Name : Ozzy ChaudRy', 'Brown leather boots', 60, 10, 1, 60, '084011557673.jpg'),
(78, 41, '<a href="https://www.facebook.com/dialog/oauth?client_id=1255638397825243&redirect_uri=http%3A%2F%2F', 'Brown leather boots', 60, 10, 1, 60, '084011557673.jpg'),
(79, 41, 'ozzy', 'Brown leather boots', 60, 10, 1, 60, '084011557673.jpg'),
(80, 41, 'usman', 'Brown leather boots', 60, 10, 1, 60, '084011557673.jpg'),
(81, 41, 'usman', 'Brown leather boots', 60, 10, 1, 60, '084011557673.jpg'),
(82, 38, 'usman', 'Black regular fit dinner suit', 160, 10, 1, 160, '46497_0039629_290.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobile` int(100) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `img` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `firstname`, `lastname`, `username`, `address`, `password`, `mobile`, `facebook`, `twitter`, `date`, `img`) VALUES
(20, 'ozzy.chaudry@gmail.com', 'ozzy', 'chaudry', 'ozzy', '102 station raod ', 'ozzy123', 2147483647, 'www.facebook.com', 'www.twitter.com', '0000-00-00 00:00:00', ''),
(22, 'ozzy.chaudry@gmail.com', 'usman', 'yousaf', 'usman', '102 station road sheffield, s94ju', 'usman123', 2147483647, 'https://www.facebook.com/UzzyChaudry', 'https://twitter.com/signup?lang=en', '2017-03-11 20:39:47', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_comments`
--
ALTER TABLE `blog_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emails`
--
ALTER TABLE `emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_sub`
--
ALTER TABLE `email_sub`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fbusers`
--
ALTER TABLE `fbusers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pageview`
--
ALTER TABLE `pageview`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_reviews`
--
ALTER TABLE `p_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `total_shares`
--
ALTER TABLE `total_shares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usercart`
--
ALTER TABLE `usercart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `blog_comments`
--
ALTER TABLE `blog_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `emails`
--
ALTER TABLE `emails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `email_sub`
--
ALTER TABLE `email_sub`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `fbusers`
--
ALTER TABLE `fbusers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `pageview`
--
ALTER TABLE `pageview`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `p_reviews`
--
ALTER TABLE `p_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `total_shares`
--
ALTER TABLE `total_shares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `usercart`
--
ALTER TABLE `usercart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
