<?php
session_start();
include 'include/config.php';
include 'include/sessionchecker.php';
?>
<!DOCTYPE html>
<html lang="en">
  
  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->
  
  <body>  
   <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
 <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Main header section -->
  <?php include 'include/mainheader.php';?> 
  <!-- / Main header section -->
 
  <!-- / Nav -->
   <?php include 'include/nav.php';?> 
  <!-- / Nav -->
   
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
    <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Checkout Page</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>                   
          <li class="active">Checkout</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="checkout">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="checkout-area">
		
		

        
            <div class="row">
              <div class="col-md-8">
                <div class="checkout-left">
                  <div class="panel-group" id="accordion">
				  
				  
				  <ul class="progress-indicator">
            <li class="info">
                <span class="bubble"></span><span class="fa fa-pencil"></span>
                Billing Details
            </li>
            
            <li class="info">
                <span class="bubble"></span><span class="fa fa-pencil"></span> Shipping Details
            </li>
            <li class="info">
                <span class="bubble"></span><span class="fa fa-pencil"></span> Order Invoice
            </li>
        </ul>
                   			
					 
                    <!-- Billing Details -->
                    <div class="panel panel-default aa-checkout-billaddress">
					
					<?php   
				  $username = $_SESSION['username'];
				  
				  $query = mysqli_query($conn,"SELECT username FROM users WHERE username='$username'");
				  
        if(mysqli_num_rows($query) > 0){
			$results = $mysqli->query("SELECT * FROM users WHERE username = '$username'");
                 if ($results) { 
               //fetch results set as object and output HTML
                 while($obj = $results->fetch_object())
                  {
			     echo '
                      
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                            Client & Billing Details
                          </a>
                        </h4>
                      </div>
					  <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                First Name : '.$obj->firstname.'
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Last Name : '.$obj->lastname.'
                              </div>
                            </div>
                          </div> 
                          <div class="row">
                            <div class="col-md-12">
                              <div class="aa-checkout-single-bill">
                                User name : '.$obj->username.'
                              </div>                             
                            </div>                            
                          </div>  
                          <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Email Address : '.$obj->email.'
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Mobile Phone : '.$obj->mobile.'
                              </div>
                            </div>
                          </div> 
                          <div class="row">
                            <div class="col-md-12">
                              <div class="aa-checkout-single-bill">
                                Address : '.$obj->address.'
                              </div>                             
                            </div>                            
                          </div>   
                          
                             
                                                              
                        </div>
                      </div>
					  ';
                       }
                      }
}
 else
    {
		
		$results = $mysqli->query("SELECT * FROM fbusers WHERE fb_username = '$username'");
                 if ($results) { 
               //fetch results set as object and output HTML
                 while($obj = $results->fetch_object())
                  {
	
				 echo '
				 
				 <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                            Client & Billing Details
                          </a>
                        </h4>
                      </div>
					  <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                         
						  <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                First Name : '.$obj->first_name.'
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Last Name : '.$obj->last_name.'
                              </div>
                            </div>
                          </div> 

                          <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Facebook ID : '.$obj->Fuid.'
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Username : '.$obj->fb_username.'
                              </div>
                            </div>
                          </div> 						  
                          
						  <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Email : '.$obj->email.'
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Phone : '.$obj->phone.'
                              </div>
                            </div>
                          </div> 
						  
						  <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Address : '.$obj->address.'
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                City : '.$obj->city.'
                              </div>
                            </div>
                          </div> 
						  
						  <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Postcode : '.$obj->postcode.'
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Country : '.$obj->country.'
                              </div>
                            </div>
                          </div> 
                           
						  
                        </div>
                      </div>
				 
				 ';
	                }
				} 
 	        }
	        ?> 
                    </div>
					<?php
					$results = $mysqli->query("SELECT * FROM billing WHERE username = '$username'");
                 if ($results) { 
               //fetch results set as object and output HTML
                 while($obj = $results->fetch_object())
                  {
					  echo '
                    <!-- Shipping Address -->
                    <div class="panel panel-default aa-checkout-billaddress">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                            Shippping Address
                          </a>
                        </h4>
                      </div>
                      <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                         
						 <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                First Name : '.$obj->firstname.'
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Last Name : '.$obj->lastname.'
                              </div>
                            </div>
                          </div> 
						  
						  <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Email : '.$obj->email.'
                              </div>                             
                            </div>
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Mobile : '.$obj->mobile.'
                              </div>
                            </div>
                          </div> 
						  
						  <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Company : '.$obj->firstname.'
                              </div>                             
                            </div>                            
                          </div> 
						  
						  <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Address : '.$obj->address.' '.$obj->city.''.$obj->pcode.' 
                              </div>                             
                            </div>                            
                          </div> 
						  
						  <div class="row">
                            <div class="col-md-6">
                              <div class="aa-checkout-single-bill">
                                Notes : '.$obj->notes.'
                              </div>                             
                            </div>                            
                          </div> 
						  
                        </div>
                      </div>
                    </div>
					';
					}
					}
					?>
					
					
                  </div>
                </div>
              </div>
			  
			  
              <div class="col-md-4">
			     <div class="checkout-right">
                  <h4>Order Summary</h4>
                  <div class="aa-order-summary-area">
				  <?php   
				 $username = $_SESSION['username'];
	             
				 
				 $results = $mysqli->query("SELECT * FROM orders WHERE username = '$username'");
                 if ($results) { 
               //fetch results set as object and output HTML
                 while($obj = $results->fetch_object())
                  {
					  $orderid = $obj->id;
					  $ordertotal = $obj->order_total;
					  $quantity = $obj->quantity;
			     echo '  
				  
                    <table class="table table-responsive">
                      <thead>
                        <tr>
                          <th>Order No</th>
                          <th>'.$obj->id.'</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Quantity <strong></strong></td>
                          <td>'.$obj->quantity.'</td>
                        </tr>
                        <tr>
                          <td>Reward Points <strong></strong></td>
                          <td>'.$obj->r_points.'</td>
                        </tr>
                        <tr>
                          <td>Discount <strong></strong></td>
                          <td>'.$obj->discount.'%</td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Discount Amount</th>
                          <td>£'.$obj->discount_p.'</td>
                        </tr>
                        
                         <tr>
                          <th>Total</th>
                          <td>£'.$obj->order_total.'</td>
                        </tr>
                      </tfoot>
                    </table>
					
					<table class="table table-responsive">
                      
                      <tbody>
                        <tr>
                          <td>Social Shares <strong></strong></td>
                          <td>'.$obj->totalshares.'</td>
                        </tr>
                        <tr>
                          <td>Social Reward Points <strong></strong></td>
                          <td>'.$obj->socialpoints.'</td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <th>Discount Amount</th>
                          <td>£'.$obj->newdiscount.'</td>
                        </tr>
                         <tr>
                          <th>Final Total</th>
                          <td>£'.$obj->neworder_total.'</td>
                        </tr>
                      </tfoot>
                    </table>
					 
					 ';
                       }
                      }
                    ?>  
                  </div>
				  <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
				  <input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="5NQB6Y8S9M4CW">
<input type="hidden" name="lc" value="GB">
<input type="hidden" name="item_name" value="name">
<input type="hidden" name="item_number" value="<?php echo $quatity; ?>">
<input type="hidden" name="amount" value="<?php echo $newordertotal; ?>">
<input type="hidden" name="currency_code" value="GBP">
<input type="hidden" name="button_subtype" value="services">
<input type="hidden" name="no_note" value="0">
<input type="hidden" name="cn" value="Add special instructions to the seller:">
<input type="hidden" name="no_shipping" value="2">
<input type="hidden" name="shipping" value="5.00">
<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynowCC_LG.gif:NonHosted">
				  
				  
                  <h4>Payment Method</h4>
                  <div class="aa-payment-method">                    
                    <label for="cashdelivery"><input type="radio" id="cashdelivery" name="optionsRadios"> Cash on Delivery </label>
                    <label for="paypal"><input type="radio" id="paypal" name="optionsRadios" checked> Via Paypal </label>
                    <img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" border="0" alt="PayPal Acceptance Mark">    
                    <input type="submit" value="Place Order" class="aa-browse-btn">                
                  </div>
				  
				  
                </div>
              </div>
            </div>
          </form>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->

   <!--   footer -->
  <?php include 'include/footer.php';?> 
  <!-- / footer -->

  <!-- Login Modal --> 
  <?php include 'include/loginmodal.php';?> 
  <!-- /Login Modal -->
    

  <!-- jQuery library -->
  <?php include 'include/jquery.php';?> 
  <!-- /jQuery library -->
    
  </body>
</html>