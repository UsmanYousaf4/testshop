<?php
session_start();
include 'include/config.php';
?>

<!DOCTYPE html>
<html lang="en">
  
   <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->
  
  <body> 
    <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
    <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
    <!-- END SCROLL TOP BUTTON -->

  <!-- Main header section -->
  <?php include 'include/mainheader.php';?> 
  <!-- / Main header section -->
 
  <!-- / Nav -->
   <?php include 'include/nav.php';?> 
  <!-- / Nav -->
   
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Contact</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>         
          <li class="active">Contact</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->
  
<!-- start contact section -->
 <section id="aa-contact">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="aa-contact-area">
           <div class="aa-contact-top">
		   
		   <span>
           <?php 
           if(isset($_GET['msg']))
           echo "<p class='alert alert-succes'> ".$_GET['msg']." </p>";
            ?>
           </span>
		   
             <h2>We are wating to assist you..</h2>
             
           </div>
           <!-- contact map -->
           <div class="aa-contact-map">
              <iframe src="https://maps.google.co.uk/maps/ms?msa=0&amp;msid=202146854789035883922.0004e16e0cef703fc02ad&amp;ie=UTF8&amp;t=m&amp;ll=53.39157,-1.42715&amp;spn=0.008957,0.042057&amp;z=15&amp;iwloc=0004e16e3495984d1cd95&amp;output=embed" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
           </div>
           <!-- Contact address -->
           <div class="aa-contact-address">
             <div class="row">
               <div class="col-md-8">
                 <div class="aa-contact-address-left">
                   <form class="comments-form contact-form" action="messagequery.php" method="post">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">                        
                          <input type="text" id="name" name="name" placeholder="Your Name" class="form-control" required />
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">                        
                          <input type="email" id="email" name="email" placeholder="Email" class="form-control" required />
                        </div>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">                        
                          <input type="text" id="subject" name="subject" placeholder="Subject" class="form-control" required />
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">                        
                          <input type="text" id="company" name="company" placeholder="Company" class="form-control" required />
                        </div>
                      </div>
                    </div>                  
                     
                    <div class="form-group">                        
                      <textarea class="form-control" rows="3" id="message" name="message" placeholder="Message"></textarea>
                    </div>
                    <button class="aa-secondary-btn" type="submit">Send</button>
                  </form>
                 </div>
               </div>
               <div class="col-md-4">
                 <div class="aa-contact-address-right">
                   <address>
                     <h4>TestShop</h4>
                     <p></p>
                     <p><span class="fa fa-home"></span>Bradford college, great horton road, UK</p>
                     <p><span class="fa fa-phone"></span>+44 114-444-4444</p>
                     <p><span class="fa fa-envelope"></span>Email: testshop@gmail.com</p>
                   </address>
                 </div>
               </div>
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>

  <!-- Subscribe section -->
  <?php include 'include/subscribe.php';?> 
  <!-- / Subscribe section -->
  
  <!--   footer -->
  <?php include 'include/footer.php';?> 
  <!-- / footer -->

  <!-- Login Modal --> 
  <?php include 'include/loginmodal.php';?> 
  <!-- /Login Modal -->
    

  <!-- jQuery library -->
  <?php include 'include/jquery.php';?> 
  <!-- /jQuery library -->
  

  </body>
</html>