FronEnd Features: 

•	Secured User Login and registration system <br>
•	Product catalogue and search <br>
•	Shopping cart and checkout<br>
•	Secured PayPal payment method<br>
•	Product reviews system<br>
•	Blob commenting system <br>
•	Loyalty rewards points system and discount <br>
•	Blogs and users commenting system <br>
•	Email subscription <br>
•	Contact us form and about us page
•	Social media Login API (Facebook, Twitter and Google+)
•	Social media share buttons for products 
•	Social media share buttons for website content
•	Social media pages linked to the main website 
•	Social media share buttons to share blogs and products reviews 


Administration Panel:

•	Manage Products (View, Add, Edit & Delete)
•	Manage Reviews (View & Delete)
•	Manage Users (View, Add, Edit, Message & Delete)
•	Manage Social media Users (View, Add, Edit, Message & Delete)
•	Manage Orders (View & Delete)
•	Manage Messages (View, Message & Delete)
•	Manage Subscriptions (View & Delete)
•	Manage Pages (View, Add, Edit & Delete)
•	Manage Blogs and comments (View, Add, Edit & Delete)
•	Manage Social media (Page Views, likes, followers and live Feeds)
•	Manage and Send Emails (Email Marketing)
•	Manage SEO tags and description (Meta keywords and description)
•	Manage administration Settings
