<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				

				<ul class="nav nav-list">
					<li class="active">
						<a href="home.php">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text"> Dashboard </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-list"></i>
							<span class="menu-text"> Products </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="">
								<a href="products.php">
									<i class="menu-icon fa fa-caret-right"></i>
									View Porducts
								</a>

								<b class="arrow"></b>
							</li>

							<li class="">
								<a href="new_product.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Add New Product
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>

					<li class="">
						<a href="reviews.php">
							<i class="menu-icon fa fa-pencil-square-o"></i>
							<span class="menu-text"> Products Reviews</span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="users.php">
							<i class="menu-icon fa fa-user"></i>
							<span class="menu-text"> Users </span>
						</a>

						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="fbusers.php">
							<i class="menu-icon fa fa-users"></i>
							<span class="menu-text"> Facebook Users </span>
						</a>

						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="orders.php">
							<i class="menu-icon fa fa-calendar"></i>
							<span class="menu-text">
								Orders
							</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="messages.php">
							<i class="menu-icon fa fa-comments"></i>
							<span class="menu-text">
								Messages
							</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="subscriptions.php">
							<i class="menu-icon fa fa-briefcase"></i>
							<span class="menu-text">
								Subscriptions
							</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="send_email.php">
							<i class="menu-icon fa fa-send"></i>
							<span class="menu-text">
								Send Email
							</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="seotags.php">
							<i class="menu-icon fa fa-tag"></i>
							<span class="menu-text">
								SEO Tags
							</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="socialmedia.php">
							<i class="menu-icon fa fa-share-alt-square"></i>
							<span class="menu-text">
								Social Media
							</span>
						</a>
						<b class="arrow"></b>
					</li>
					
					<li class="">
						<a href="shares.php">
							<i class="menu-icon fa fa-share-alt-square"></i>
							<span class="menu-text">
								Social Media Shares
							</span>
						</a>
						<b class="arrow"></b>
					</li>

					<li class="">
						<a href="#" class="dropdown-toggle">
							<i class="menu-icon fa fa-file-o"></i>
							<span class="menu-text"> Pages </span>

							<b class="arrow fa fa-angle-down"></b>
						</a>

						<b class="arrow"></b>

						<ul class="submenu">
							<li class="">
								<a href="post_blog.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Post a Blog
								</a>

								<b class="arrow"></b>
							</li>
							<li class="">
								<a href="blogs.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Blogs
								</a>

								<b class="arrow"></b>
							</li>
							<li class="">
								<a href="blog_comments.php">
									<i class="menu-icon fa fa-caret-right"></i>
									Blog Comments
								</a>

								<b class="arrow"></b>
							</li>
							<li class="">
								<a href="about.php?var=aboutus">
									<i class="menu-icon fa fa-caret-right"></i>
									About us
								</a>

								<b class="arrow"></b>
							</li>
						</ul>
					</li>
					
				<li class="">
						<a href="settings.php">
							<i class="menu-icon fa fa-cogs"></i>
							<span class="menu-text">
								Settings
							</span>
						</a>
						<b class="arrow"></b>
					</li>
					
				</ul><!-- /.nav-list -->

				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>
			</div>