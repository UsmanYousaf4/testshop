<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();

 /* inlcuded configration file to  connect the database  */
include 'include/config.php';

 /* Session checker file included to check whether the user session exits or not */
/*include 'include/sessionchecker.php';*/

?>

<!DOCTYPE html>
<html lang="en">


  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->

	<body class="no-skin">
		
		<!--  Navbar section -->
        <?php include 'include/navbar.php';?>    
        <!-- /Navbar section -->
		

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
        <!--  Sidebar section -->
        <?php include 'include/sidebar.php';?>    
        <!-- /Sidebar section -->

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Email</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						

						<div class="page-header">
							<h1>
								Manage
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Send Email
								</small>
							</h1>
						</div><!-- /.page-header -->
                    
						<div class="row">
						
						<span>
                        <?php 
                        if(isset($_GET['msg']))
                        echo "<p class ='alert alert-success'> ".$_GET['msg']." </p>";
                        ?>
                        </span>
							                 
							<form id="form1" class="stdform" method="post" action="emailusers_query.php" enctype="multipart/form-data" >
							
                            <div class="par control-group">
                                    <label class="control-label" for="firstname">Email</label>
                                <div class="controls"><input type="text" name="email" id="email" class="input-large" value="" /></div>
                            </div>
                                                       
                             <div class="par control-group">
                                    <label class="control-label" for="firstname">Content</label>
                                <div class="controls"><textarea name="comment" cols="100" rows="25" > 
                                this is the test form  to send email to all users 
                                
                                <br/>
                                <p>
                                TestShop, <br/>
                                Sheffield, UK<br/>
                                Phone:0000 111 2222<br/>
                                Email:info@http://testhop.comeze.com/<br/>
                                Web:http://testhop.comeze.com/<br/></p>

								</textarea></div>
                            </div>
                                             
                            <p class="stdformbutton">
                            <input type="hidden" id="id" name="id" value="" />
                            <input type="submit" class="btn btn-primary" value="Send Email" />
                            </p>
						   </form>
														
					    </div><!-- /.row -->

								<div class="hr hr32 hr-dotted"></div>

								
							</div><!-- /.row -->

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			 <!--  Footer section -->
             <?php include 'include/Footer.php';?>    
             <!-- /Footer section -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

  <!--  JS section -->
  <?php include 'include/js.php';?>    
  <!-- /JS section -->
	</body>
</html>
