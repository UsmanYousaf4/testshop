<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();

 /* inlcuded configration file to  connect the database  */
include 'include/config.php';

 /* Session checker file included to check whether the user session exits or not */
/*include 'include/sessionchecker.php';*/

?>

<!DOCTYPE html>
<html lang="en">


  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->

	<body class="no-skin">
		
		<!--  Navbar section -->
        <?php include 'include/navbar.php';?>    
        <!-- /Navbar section -->
		

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
        <!--  Sidebar section -->
        <?php include 'include/sidebar.php';?>    
        <!-- /Sidebar section -->

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Social Media Shares</li>
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="nav-search" id="nav-search">
							<form class="form-search" id="form-search" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
								<span class="input-icon">
									<input type="text" placeholder="Search ..." id="search" name="search" class="nav-search-input" id="nav-search-input"  />
									<i class="ace-icon fa fa-search nav-search-icon" onclick="document.getElementById('form-search').submit();"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->

						<div class="page-header">
							<h1>
								Manage
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Social Media Shares
								</small>
							</h1>
						</div><!-- /.page-header -->
                    
						<div class="row">
							
							<div class="col-xs-12">
										<h3 class="header smaller lighter blue">All Social Shares from Users</h3>

										<div class="clearfix">
											<div class="pull-right tableTools-container"></div>
										</div>
										 <span>
   <?php 
   if(isset($_GET['msg']))
  echo "<p class='alert alert-danger'> ".$_GET['msg']." </p>";
  ?>
  </span>
										<div class="table-header">
											Results for "Customers Socail Media Shares"
										</div>

										<!-- div.table-responsive -->

										<!-- div.dataTables_borderWrap -->
										<div>
											<table id="dynamic-table" class="table table-striped table-bordered table-hover">
											
												<thead>
													<tr>
														<th class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>
														</th>
														<th>
														<i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
														 ID
														</th>
														<th>
														<i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
														Custumer
														</th>
														<th>
														<i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
														Total Shares
														</th>

														<th>
															<i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
															Reward Points
														</th>
														<th>
															<i class="ace-icon fa fa-clock-o bigger-110 hidden-480"></i>
															Discount
														</th>
														
														<th class="hidden-480">Status</th>

														<th> Actions</th>
													</tr>
												</thead>

												<tbody>
												
												<?php
		
		$con=mysqli_connect("localhost","root","","shop_db");
        if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
		
		
		
		
		$result2 = mysqli_query($con,"SELECT sum(shares),sum(points) FROM total_shares");
		
		
		while ($rows = mysqli_fetch_array($result2)) {
			
			
			$finaltotal= $rows['sum(shares)'];
			$totalpoints= $rows['sum(points)'];
			
			if ($totalpoints <=  "100") {
			  $discount_value = '10';
			 
            
            } else if ($totalpoints <=  "200") {
			  $discount_value = '20';
			  
          	}
			else {
             $discount_value = '0';
            }
			
			
			
			
			
            ?>
													
													<tr>
														<td class="center">
															<label class="pos-rel">
																<input type="checkbox" class="ace" />
																<span class="lbl"></span>
															</label>
														</td>

														<td>
															<a href="#">#</a>
														</td>
														<td>Ozzy</td>
														<td class="hidden-480"><?php echo $finaltotal; ?></td>
														<td><?php echo $totalpoints; ?></td>
														<td><?php echo $discount_value; ?>%</td>
														

														<td class="hidden-480">
															<span class="label label-sm label-warning">Expiring</span>
														</td>

														<td>
															<div class="hidden-sm hidden-xs action-buttons">
																<a class="blue" href="#">
																	<i class="ace-icon fa fa-search-plus bigger-130"></i>
																</a>

																<a class="green" href="#">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>

																<a class="red" href="delete_order.php?id='.$obj->id.'">
																	<i class="ace-icon fa fa-trash-o bigger-130"></i>
																</a>
															</div>

															
														</td>
													</tr>

																	<?php }

	              ?> 
												</tbody>
											</table>
										</div>
									</div>
														
							</div><!-- /.row -->

								<div class="hr hr32 hr-dotted"></div>

								
							</div><!-- /.row -->

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			 <!--  Footer section -->
             <?php include 'include/Footer.php';?>    
             <!-- /Footer section -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

  <!--  JS section -->
  <?php include 'include/js.php';?>    
  <!-- /JS section -->
	</body>
</html>
