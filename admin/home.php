<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();
 /* inlcuded configration file to  connect the database  */
include 'include/config.php';
 /* Session checker file included to check whether the user session exits or not */
include 'include/sessionchecker.php';
?>

<!DOCTYPE html>
<html lang="en">
  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->

	<body class="no-skin">
    <!--  FB SDK section -->
    <?php include 'include/fbsdk.php';?>    
    <!-- /FB SDK section -->

    <!--  Navbar section -->
    <?php include 'include/navbar.php';?>    
    <!-- /Navbar section -->
		

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
        <!--  Sidebar section -->
        <?php include 'include/sidebar.php';?>    
        <!-- /Sidebar section -->

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Dashboard</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						

						<div class="page-header">
							<h1>
								Dashboard
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									overview &amp; stats
								</small>
							</h1>
						</div><!-- /.page-header -->
                    
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
							
							<div class="row">
									<div class="space-6"></div>
 <div class="container">
    <div class="row">
        
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-6 col-md-6">
                          <a href="products.php" class="btn btn-danger btn-lg" role="button"><span class="glyphicon glyphicon-list-alt"></span> <br/>Products</a>
                          <a href="new_product.php" class="btn btn-warning btn-lg" role="button"><span class="glyphicon glyphicon-bookmark"></span> <br/>Add new</a>
                          <a href="orders.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-signal"></span> <br/>Orders</a>
                          <a href="messages.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-comment"></span> <br/>Messages</a>
                        </div>
                        <div class="col-xs-6 col-md-6">
                          <a href="users.php" class="btn btn-success btn-lg" role="button"><span class="glyphicon glyphicon-user"></span> <br/>Users</a>
                          <a href="pages.php" class="btn btn-info btn-lg" role="button"><span class="glyphicon glyphicon-file"></span> <br/>Pages</a>
                          <a href="Send_email.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-picture"></span> <br/>Send Email</a>
                          <a href="reviews.php" class="btn btn-primary btn-lg" role="button"><span class="glyphicon glyphicon-tag"></span> <br/>Reviews</a>
					   </div>
					   
					  				   
					   <div class="col-xs-6 col-md-6">
					   
					                   <div class="infobox infobox-orange2">
											<div class="infobox-chart">
												<span class="sparkline" data-values="196,128,202,177,154,94,100,170,224"></span>
											</div>
											<div class="infobox-data">
											<?php											
                                            $results4 = $mysqli->query("SELECT * FROM pageview");
                                            if ($results4) { 
                                            //fetch results set as object and output HTML
                                            while($obj = $results4->fetch_object())                                           
                                            {                                                                                           
                                            ?>
												<span class="infobox-data-number"><?php echo $obj->counts;  ?></span>
												<div class="infobox-content">pageviews</div>
											<?php
											}
											}
                                            ?>
											</div>
											<div class="badge badge-success">
												7.2%
												<i class="ace-icon fa fa-arrow-up"></i>
											</div>
										</div>
										
										<div class="infobox infobox-blue">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-twitter"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number">11</span>
												<div class="infobox-content">Twitter followers</div>
											</div>

											<div class="badge badge-success">
												+32%
												<i class="ace-icon fa fa-arrow-up"></i>
											</div>
										</div>
										
										<div class="infobox infobox-blue3">
											<div class="infobox-icon">
                                            <i class="ace-icon fa fa-facebook"></i>											
											</div>
											<div class="infobox-data">
											<div class="fb-like" data-href="https://www.facebook.com/Test-Shop-1877399189205162/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true"></div>
											<div class="infobox-content">Facebook Likes</div>
											</div>
										</div>
										
										<div class="infobox infobox-red">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-google-plus"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number">11</span>
												<div class="infobox-content">Google+ followers</div>
											</div>

											<div class="badge badge-success">
												+32%
												<i class="ace-icon fa fa-arrow-up"></i>
											</div>
										</div>		

                                        <div class="infobox infobox-blue3">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-facebook"></i>
											</div>

											<div class="infobox-data">
											<?php
                                            $sql6="SELECT * FROM fbusers";
                                            if ($result6=mysqli_query($conn,$sql6))
                                            {  
                                            $rowcount=mysqli_num_rows($result6);                                             
                                            ?>
											
												<span class="infobox-data-number"><?php echo $rowcount;  ?> </span>
												<div class="infobox-content">Facebook Users</div>
											<?php
											}
                                            ?>
											</div>
										</div>	

                                            <div class="infobox infobox-orange">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-share"></i>
											</div>

											<div class="infobox-data">
											<?php
                                            $sql6="SELECT * FROM total_shares";
                                            if ($result6=mysqli_query($conn,$sql6))
                                            {  
                                            $rowcount=mysqli_num_rows($result6);                                             
                                            ?>
											
												<span class="infobox-data-number"><?php echo $rowcount;  ?> </span>
												<div class="infobox-content">Social Media Shares </div>
											<?php
											}
                                            ?>
											</div>
										</div>											
										
										<div class="infobox infobox-black">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-inbox"></i>
											</div>

											<div class="infobox-data">
											<?php
                                            $sql5="SELECT * FROM email_sub";
                                            if ($result5=mysqli_query($conn,$sql5))
                                            {  
                                            $rowcount=mysqli_num_rows($result5);                                             
                                            ?>
											
												<span class="infobox-data-number"><?php echo $rowcount;  ?> </span>
												<div class="infobox-content">Email Subscribers</div>
											<?php
											}
                                            ?>
											</div>
										</div>
										
										
                                        <div class="infobox infobox-grey">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-users"></i>
											</div>

											<div class="infobox-data">
											<?php
                                            $sql6="SELECT * FROM users";
                                            if ($result6=mysqli_query($conn,$sql6))
                                            {  
                                            $rowcount=mysqli_num_rows($result6);                                             
                                            ?>
											
												<span class="infobox-data-number"><?php echo $rowcount;  ?> </span>
												<div class="infobox-content">Website Members</div>
											<?php
											}
                                            ?>
											</div>
										</div>										
																				
                                        <div class="infobox infobox-green">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-pencil-square-o"></i>
											</div>
                                            
											<div class="infobox-data">
											<?php                                            
                                            $sql="SELECT * FROM emails";
                                            if ($result=mysqli_query($conn,$sql))
                                            {  
                                            $rowcount=mysqli_num_rows($result);                                             
                                            ?>
											
												<span class="infobox-data-number"><?php echo $rowcount;  ?> </span>
												<div class="infobox-content">Products Reviews</div>
											<?php
											}
                                            ?>
											</div>
											<div class="stat stat-success">8%</div>
										</div>	

										<div class="infobox infobox-pink">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-shopping-cart"></i>
											</div>                                            											
											<div class="infobox-data">
											<?php
                                            $sql2="SELECT * FROM orders";
                                            if ($result2=mysqli_query($conn,$sql2))
                                            {  
                                            $rowcount=mysqli_num_rows($result2);                                             
                                            ?>
												<span class="infobox-data-number"><?php echo $rowcount;  ?></span>
												<div class="infobox-content">new orders</div>
											<?php
											}
                                            ?>
												
											</div>
											<div class="stat stat-important">4%</div>
										</div>

										

										<div class="infobox infobox-blue2">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-comments"></i>
											</div>

											<div class="infobox-data">
											<?php
                                            $sql3="SELECT * FROM messages";
                                            if ($result3=mysqli_query($conn,$sql3))
                                            {  
                                            $rowcount=mysqli_num_rows($result3);                                             
                                            ?>
											
												<span class="infobox-data-number"><?php echo $rowcount;  ?> </span>
												<div class="infobox-content">Total Messages</div>
											<?php
											}
                                            ?>
											</div>
										</div>
										
										<div class="infobox infobox-black">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-comments"></i>
											</div>

											<div class="infobox-data">
											<?php
                                            $sql7="SELECT * FROM blog_comments";
                                            if ($result7=mysqli_query($conn,$sql7))
                                            {  
                                            $rowcount=mysqli_num_rows($result7);                                             
                                            ?>
											
												<span class="infobox-data-number"><?php echo $rowcount;  ?> </span>
												<div class="infobox-content">Blog Comments</div>
											<?php
											}
                                            ?>
											</div>
										</div>

										<div class="space-6"></div>

										<div class="infobox infobox-green infobox-small infobox-dark">
											<div class="infobox-progress">
												<div class="easy-pie-chart percentage" data-percent="61" data-size="39">
													<span class="percent">61</span>%
												</div>
											</div>

											<div class="infobox-data">
												<div class="infobox-content">Task</div>
												<div class="infobox-content">Completion</div>
											</div>
										</div>

										<div class="infobox infobox-blue infobox-small infobox-dark">
											<div class="infobox-chart">
												<span class="sparkline" data-values="3,4,2,3,4,4,2,2"></span>
											</div>

											<div class="infobox-data">
												<div class="infobox-content">Earnings</div>
												<div class="infobox-content">$32,000</div>
											</div>
										</div>

										<div class="infobox infobox-grey infobox-small infobox-dark">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-download"></i>
											</div>

											<div class="infobox-data">
												<div class="infobox-content">Downloads</div>
												<div class="infobox-content">1,205</div>
											</div>
										</div>
					   </div>
					   
					   <div class="col-xs-6 col-md-6">
                          <div class="widget-box">
											<div class="widget-body">
												<div class="widget-main">
																								

													<div class="clearfix">
														<div class="grid3">
															<span class="grey">
																<i class="ace-icon fa fa-facebook-square fa-2x blue"></i>
																&nbsp; likes
															</span>
															<h4 class="bigger pull-right">1,255</h4>
														</div>

														<div class="grid3">
															<span class="grey">
																<i class="ace-icon fa fa-twitter-square fa-2x purple"></i>
																&nbsp; tweets
															</span>
															<h4 class="bigger pull-right">941</h4>
														</div>

														<div class="grid3">
															<span class="grey">
																<i class="ace-icon fa fa-pinterest-square fa-2x red"></i>
																&nbsp; pins
															</span>
															<h4 class="bigger pull-right">1,050</h4>
														</div>
													</div>
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
					   </div>
					   
					   <div class="col-xs-6 col-md-6">
                          <div class="widget-box">
											<div class="widget-body">
												<div class="widget-main">
																								

													<!--/Twitter Widget-->
                         <h3 style="padding-top:20px">Twitter Live Feeds</h3>
                         <div id="twitter">       
                                
								<a class="twitter-timeline" href="https://twitter.com/testshopEcom">Tweets by TwitterDev</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
								
								</div>
                            <!--/Twitter Widget-->
												</div><!-- /.widget-main -->
											</div><!-- /.widget-body -->
										</div><!-- /.widget-box -->
					   </div>
                    </div>
                    
                </div>
            
        </div>
    </div>
</div>
									

									<div class="vspace-12-sm"></div>

									
							</div><!-- /.row -->

								<div class="hr hr32 hr-dotted"></div>

								
							</div><!-- /.row -->

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			 <!--  Footer section -->
             <?php include 'include/Footer.php';?>    
             <!-- /Footer section -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

  <!--  JS section -->
  <?php include 'include/js.php';?>    
  <!-- /JS section -->
	</body>
</html>
