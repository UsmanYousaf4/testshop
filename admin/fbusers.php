<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();

 /* inlcuded configration file to  connect the database  */
include 'include/config.php';

 /* Session checker file included to check whether the user session exits or not */
/*include 'include/sessionchecker.php';*/

?>

<!DOCTYPE html>
<html lang="en">


  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->

	<body class="no-skin">
		
		<!--  Navbar section -->
        <?php include 'include/navbar.php';?>    
        <!-- /Navbar section -->
		

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
        <!--  Sidebar section -->
        <?php include 'include/sidebar.php';?>    
        <!-- /Sidebar section -->

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="#">Home</a>
							</li>
							<li class="active">Facebook Users</li>
						</ul><!-- /.breadcrumb -->

						
					</div>

					<div class="page-content">
						
                       <div class="nav-search" id="nav-search">
							<form class="form-search" id="form-search" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
								<span class="input-icon">
									<input type="text" placeholder="Search ..." id="search" name="search" class="nav-search-input" id="nav-search-input"  />
									<i class="ace-icon fa fa-search nav-search-icon" onclick="document.getElementById('form-search').submit();"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
						<div class="page-header">
							<h1>
								Manage
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									All Facebook Users
								</small>
							</h1>
						</div><!-- /.page-header -->
                    
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
									
							
							<div class="row">
							<span>
                            <?php 
                            if(isset($_GET['msg']))
                            echo "<p class ='alert alert-success'> ".$_GET['msg']." </p>";
                            ?>
                            </span>
							<?php
							if(empty($_POST['search'])) 
                             { 
						      $results = $mysqli->query("SELECT * FROM fbusers ORDER BY id DESC");
							 }
							else {
								
								$results =  $results = $mysqli->query("SELECT * FROM fbusers WHERE first_name LIKE '%$_POST[search]%'");
								
							}
							
							
    if ($results) { 
	
        //fetch results set as object and output HTML
        while($obj = $results->fetch_object())
        {
			echo '
							<div class="table-detail">
															<div class="row">
																<div class="col-xs-12 col-sm-2">
																	<div class="text-center">
																		<img height="150" class="thumbnail inline no-margin-bottom" alt="Domain Owners Avatar" src="assets/images/avatars/profile-pic.jpg" />
																		<br />
																		<div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
																			<div class="inline position-relative">
																				<a class="user-title-label" href="#">
																					<i class="ace-icon fa fa-circle light-green"></i>
																					&nbsp;
																					<span class="white">'.$obj->first_name.'</span>
																				</a>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="col-xs-12 col-sm-7">
																	<div class="space visible-xs"></div>

																	<div class="profile-user-info profile-user-info-striped">
																		
																		<div class="profile-info-row">
																			<div class="profile-info-name"> Facebook ID </div>

																			<div class="profile-info-value">
																				<span>'.$obj->Fuid.'</span>
																			</div>
																		</div>
																		
																		<div class="profile-info-row">
																			<div class="profile-info-name"> Username </div>

																			<div class="profile-info-value">
																				<span>'.$obj->fb_username.'</span>
																			</div>
																		</div>
																		
																		<div class="profile-info-row">
																			<div class="profile-info-name"> First Name </div>

																			<div class="profile-info-value">
																				<span>'.$obj->first_name.'</span>
																			</div>
																		</div>

																		<div class="profile-info-row">
																			<div class="profile-info-name"> Last Name </div>

																			<div class="profile-info-value">
																				<i class="fa fa-map-marker light-orange bigger-110"></i>
																				<span>'.$obj->last_name.'</span>
																			</div>
																		</div>


																		<div class="profile-info-row">
																			<div class="profile-info-name"> Email </div>

																			<div class="profile-info-value">
																				<span>'.$obj->email.'</span>
																			</div>
																		</div>

																		<div class="profile-info-row">
																			<div class="profile-info-name"> Address </div>

																			<div class="profile-info-value">
																				<span>'.$obj->address.'</span>
																			</div>
																		</div>
																		
																																				
																		<div class="profile-info-row">
																			<div class="profile-info-name"> Phone </div>

																			<div class="profile-info-value">
																				<span>'.$obj->phone.'</span>
																			</div>
																		</div>
																		
																		<div class="profile-info-row">
																			<div class="profile-info-name"> City </div>

																			<div class="profile-info-value">
																				<span>'.$obj->city.'</span>
																			</div>
																		</div>

																		<div class="profile-info-row">
																			<div class="profile-info-name"> Social Media </div>

																			<div class="profile-info-value">
																				

							<a href="">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							
						</span>
																			</div>
																		</div>
																	</div>
																</div>

																<div class="col-xs-12 col-sm-3">
																	<div class="space visible-xs"></div>
																	<h4 class="header blue lighter less-margin">Send a message to '.$obj->first_name.''.$obj->last_name.'</h4>

																	<div class="space-6"></div>
																	
																	
																	
																	
						           <form role="form" action="message_query.php" method="post" enctype="multipart/form-data">
								
								   <input type="hidden" id="id" name="id" value="'.$obj->id.'" class="col-xs-10 col-sm-5" />
								   <input type="hidden" id="email" name="email" value="'.$obj->email.'" class="col-xs-10 col-sm-5" />
									
										<fieldset>
									<textarea name="comment" rows="2" cols="5"></textarea>
									    </fieldset>
									<div class="hr hr-dotted"></div>
                                   <div class="clearfix">									
								   <button class="pull-right btn btn-sm btn-primary btn-white btn-round" type="submit">
									Send Message
								   <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i>
								   </button>
									</div>
									</form>
																	
																	
																</div>
															</div>
														</div>
							';
        }
    
    }
    ?>
									                    
														
							</div><!-- /.row -->

								<div class="hr hr32 hr-dotted"></div>

								
							</div><!-- /.row -->

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			 <!--  Footer section -->
             <?php include 'include/Footer.php';?>    
             <!-- /Footer section -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

  <!--  JS section -->
  <?php include 'include/js.php';?>    
  <!-- /JS section -->
	</body>
</html>
