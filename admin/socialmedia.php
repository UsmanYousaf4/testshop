<?php
 /* Following fucntion will start rthe session automatically after user login */
session_start();

 /* inlcuded configration file to  connect the database  */
include 'include/config.php';

 /* Session checker file included to check whether the user session exits or not */
include 'include/sessionchecker.php';

?>
<!DOCTYPE html>
<html lang="en">


  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->

	<body class="no-skin">
    <!--  FB SDK section -->
    <?php include 'include/fbsdk.php';?>    
    <!-- /FB SDK section -->
		
		<!--  Navbar section -->
        <?php include 'include/navbar.php';?>    
        <!-- /Navbar section -->
		

		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>
			
        <!--  Sidebar section -->
        <?php include 'include/sidebar.php';?>    
        <!-- /Sidebar section -->

			<div class="main-content">
				<div class="main-content-inner">
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">
						<ul class="breadcrumb">
							<li>
								<i class="ace-icon fa fa-home home-icon"></i>
								<a href="home.php">Home</a>
							</li>
							<li class="active">Social Media Accounts</li>
						</ul><!-- /.breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->
					</div>

					<div class="page-content">
						

						<div class="page-header">
							<h1>
								Manage
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Social Media Accounts
								</small>
							</h1>
						</div><!-- /.page-header -->
                    
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
							
							<div class="row">
									<div class="space-6"></div>
 <div class="container">
    <div class="row">
        
                <div class="panel-body">
                    <div class="row">
                        					   				  				   
					   <div class="col-xs-6 col-md-6">
					   
                                            <div class="infobox infobox-orange2">
											<div class="infobox-chart">
												<span class="sparkline" data-values="196,128,202,177,154,94,100,170,224"></span>
											</div>
											<div class="infobox-data">
											<?php
                                            $results4 = $mysqli->query("SELECT * FROM pageview");
                                            if ($results4) { 
                                            //fetch results set as object and output HTML
                                            while($obj = $results4->fetch_object())                                           
                                            {                                                                                           
                                            ?>
												<span class="infobox-data-number"><?php echo $obj->counts;  ?></span>
												<div class="infobox-content">pageviews</div>
											<?php
											}
											}
                                            ?>
											</div>
											<div class="badge badge-success">
												7.2%
												<i class="ace-icon fa fa-arrow-up"></i>
											</div>
										</div>

										<div class="infobox infobox-blue">
											<div class="infobox-icon">
												<i class="ace-icon fa fa-twitter"></i>
											</div>

											<div class="infobox-data">
												<span class="infobox-data-number">11</span>
												<div class="infobox-content">new followers</div>
											</div>

											<div class="badge badge-success">
												+32%
												<i class="ace-icon fa fa-arrow-up"></i>
											</div>
										</div>

										<div class="infobox infobox-blue">
											<div class="infobox-icon">
                                            <i class="ace-icon fa fa-facebook"></i>											
											</div>
											<div class="infobox-data">
											<div class="fb-like" data-href="https://www.facebook.com/Test-Shop-1877399189205162/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true"></div>
											<div class="infobox-content">Facebook Likes</div>
											</div>
										</div>

										<div class="infobox infobox-blue2">
											<div class="infobox-progress">
												<div class="easy-pie-chart percentage" data-percent="42" data-size="46">
													<span class="percent">42</span>%
												</div>
											</div>

											<div class="infobox-data">
												<span class="infobox-text">traffic used</span>

												<div class="infobox-content">
													<span class="bigger-110">~</span>
													58GB remaining
												</div>
											</div>
										</div>

										<div class="space-6"></div>
										<p>Facebook Live page</p>
										<div class="fb-page" width="580px" height="300" data-href="https://www.facebook.com/Test-Shop-1877399189205162/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Test-Shop-1877399189205162/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Test-Shop-1877399189205162/">Test Shop</a></blockquote></div>
                                        <div class="space-6"></div>
										
										<div class="col-xs-6 col-md-6">
										<p>Twitter Live Feeds</p>
                                        <div class="widget-box" width="580px" height="300" >
											
												
																								

													<!--/Twitter Widget-->
                                        <h3 style="padding-top:20px">Twitter Live Feeds</h3>
                                       <div id="twitter">  
									   
                                      <a class="twitter-timeline" href="https://twitter.com/testshopEcom">Tweets by TwitterDev</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
								<a class="twitter-share-button"
  href="https://twitter.com/intent/tweet?text=Hello%20world"
  data-size="large">
Tweet</a>
								

						   </div>
                            <!--/Twitter Widget-->
												
											
										</div><!-- /.widget-box -->
					   </div>
					   </div>
					   
					   
					   
					   
                    </div>
                    
                </div>
            
        </div>
    </div>
</div>
									

									<div class="vspace-12-sm"></div>

									
							</div><!-- /.row -->

								<div class="hr hr32 hr-dotted"></div>

								
							</div><!-- /.row -->

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			 <!--  Footer section -->
             <?php include 'include/Footer.php';?>    
             <!-- /Footer section -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

  <!--  JS section -->
  <?php include 'include/js.php';?>    
  <!-- /JS section -->
	</body>
</html>
