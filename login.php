<!DOCTYPE html>
<?php
session_start();
include 'include/config.php';

?>

<html lang="en">
 
  <body>
  
  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->
  
   <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
 <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->
  <!-- Main header section -->
  <?php include 'include/mainheader.php';?> 
  <!-- / Main header section -->
 
  <!-- / Nav -->
   <?php include 'include/nav.php';?> 
  <!-- / Nav -->  
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
    <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
    <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Login Page</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>                   
          <li class="active">Login</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="aa-myaccount">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
        <div class="aa-myaccount-area">         
            <div class="row">
              <div class="col-md-6">
                <div class="aa-myaccount-login">
                <h4>
                <span>
                <?php 
                if(isset($_GET['msg']))
                echo "<p class='alert alert-succes'> ".$_GET['msg']." </p>";
                ?>
                </span>
                Login to your account
                </h4>
                <label>
                <?php 
                if (isset($_GET['registration']) && $_GET['registration']=='unavailable')
                echo "<p class='alert alert-danger'>Username and Password Invalid Try Again </p>";
                ?>
                </label>
                
                 <form action="loginquery.php" method="post" class="aa-login-form">
                  <label for="">Username or Email address<span>*</span></label>
                   <input type="text" id="username" name="username" placeholder="Username or email">
                   <label for="">Password<span>*</span></label>
                    <input type="password" id="password" name="password" placeholder="Password">
                    <button type="submit" class="aa-browse-btn">Login</button>
                    <label class="rememberme" for="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>
                    <p class="aa-lost-password"><a href="#">Lost your password?</a></p>
                  </form>
                </div>
              </div>
              <div class="col-md-6">
                <div class="aa-myaccount-register">                 
                 <h4>Login with Social Media Accounts</h4>
				  <?php if ($_SESSION['FBID']): ?>      <!--  After user login  -->
				  
				  
				  
                 <div class="col-sm-4 social-buttons">
				 <h1>Hello <?php echo $_SESSION['FULLNAME']; ?></h1>
				 
				 <?php else: ?> 
                 <a href="fbconfig.php" class="btn  btn-social btn-lg btn-facebook">
                 <i class="fa fa-facebook"></i> Sign in with Facebook
                 </a>
                 <a class="btn  btn-social btn-lg btn-twitter">
                 <i class="fa fa-twitter"></i> Sign in with Twitter
                 </a>
                 <a class="btn  btn-social btn-lg btn-google">
                 <i class="fa fa-google"></i> Sign in with Google
                 </a>
                 <?php endif ?>
                 </div>
                <!-- <form action="" class="aa-login-form">
                    <label for="">Username or Email address<span>*</span></label>
                    <input type="text" placeholder="Username or email">
                    <label for="">Password<span>*</span></label>
                    <input type="password" placeholder="Password">
                    <button type="submit" class="aa-browse-btn">Register</button>                    
                  </form> -->
                </div>
              </div>
            </div>          
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->

  <!-- Subscribe section -->
  <?php include 'include/subscribe.php';?> 
  <!-- / Subscribe section -->
  
  <!--   footer -->
  <?php include 'include/footer.php';?> 
  <!-- / footer -->

  <!-- Login Modal --> 
  <?php include 'include/loginmodal.php';?> 
  <!-- /Login Modal -->
    

  <!-- jQuery library -->
  <?php include 'include/jquery.php';?> 
  <!-- /jQuery library -->

  

  </body>
</html>