<?php
session_start();
include 'include/config.php';
include 'include/sessionchecker.php';
?>

<!DOCTYPE html>
<html lang="en">
  
  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->
  
  <body>
   
   <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
 <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Main header section -->
  <?php include 'include/mainheader.php';?> 
  <!-- / Main header section -->
 
  <!-- / Nav -->
   <?php include 'include/nav.php';?> 
  <!-- / Nav -->
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Cart Page</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>                   
          <li class="active">Cart</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

 <!-- Cart view section -->
 <section id="cart-view">
   <div class="container">
     <div class="row">
       <div class="col-md-12">
         <div class="cart-view-area">
           <div class="cart-view-table">
             <form action="">
			 <?php 
             if(isset($_GET['msg']))
             echo "<p class='alert alert-danger'> ".$_GET['msg']." </p>";
             ?>
			 <?php 
             if(isset($_GET['msg1']))
             echo "<p class='alert alert-success'> ".$_GET['msg1']." </p>";
             ?>
               <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th>Product</th>
                        <th>Price</th>
						<th>Reward Points</th>
                        <th>Quantity</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody>
					
					<?php

                        $username=  $_SESSION['username'];
                        $results = $mysqli->query("select * from usercart where username= '$username' ORDER BY id desc ");	
						if ($results) { 
	
                        //fetch results set as object and output HTML
                        while($obj = $results->fetch_object())
                        {
			            $points = $obj->points;
					  ?>
					
                      <tr>
                        <td><a class="remove" href="product_delete.php?id=<?php echo $obj->id; ?>"><fa class="fa fa-close"></fa></a></td>
                        <td><a href="#"><img src="img/man/<?php echo $obj->img; ?>" alt="img"></a></td>
                        <td><a class="aa-cart-title" href="#"><?php echo $obj->ptitle; ?></a></td>
                        <td>£<?php echo $obj->price; ?></td>
						<td><?php echo $obj->points; ?></td>
                        <td><input class="aa-cart-quantity" type="number" value="1"></td>
                        <td>£<?php echo $obj->totalcart; ?></td>
                      </tr>                                          
					  
					 <?php
					  }
                    }
	               ?>
				   
                      </tbody>
                  </table>
                </div>
             </form>
			 
			 <?php
		
		$result1 = mysqli_query($con,"SELECT sum(totalcart),sum(points),sum(quantity) FROM usercart WHERE `username` = '$username'");
		
		
		while ($rows = mysqli_fetch_array($result1)) {
			
			$quatity = $rows['sum(quantity)'];
			$finaltotal= $rows['sum(totalcart)'];
			$totalpoints= $rows['sum(points)'];
			
			if ($totalpoints <=  "100") {
			  $percent = '10';
			 $discount_value = ($finaltotal / 100) * $percent;
            
            } else if ($points <=  "200") {
			  $percent = '20';
			  $discount_value = ($finaltotal / 100) * $percent;
          
			}
			else {
             $discount_value = '0';
            }
			
			$new_price = $finaltotal - $discount_value;
            ?>
			
             <!-- Cart Total view -->
             <div class="cart-view-total">
               <h4>Cart Totals</h4>
               <table class="aa-totals-table">
                 <tbody>
                   <tr>
                     <th>Subtotal</th>
                     <td>£<?php echo $finaltotal; ?></td>
                   </tr>
                   <tr>
                     <th>Reward Points</th>
                     <td><?php echo $totalpoints ; ?></td>
                   </tr>
				   <tr>
                     <th>Discount price</th>
                     <td>£<?php echo $discount_value; ?></td>
                   </tr>
				   <tr>
                     <th>Total</th>
                     <td>£<?php echo $new_price; ?></td>
                   </tr>
                 </tbody>
               </table>
			   
			    <?php }

	              ?>      
<!-- Apply Social Share discount -->
		   
		   <?php
		
		$result2 = mysqli_query($con,"SELECT sum(shares),sum(points) FROM total_shares WHERE `user` = '$username'");
		
		
		while ($rows = mysqli_fetch_array($result2)) {
			
			
			$finaltotal1= $rows['sum(shares)'];
			$totalpoints1= $rows['sum(points)'];
			
			if ($totalpoints1 <=  "100") {
			  $percent = '5';
			 $discount_value1 = ($new_price / 100) * $percent;
			 
            
            } else if ($totalpoints1 <=  "200") {
			  $percent = '10';
			 $discount_value1 = ($new_price / 100) * $percent;
			  
          	}
			else {
             $discount_value1 = '0';
            }
			
			$newfinal_price = $new_price - $discount_value1;
			
            ?>
		   
		   
             <div class="cart-view-total">
               <h4>Social Share Discount</h4>
               <table class="aa-totals-table">
                 <tbody>
                   <tr>
                     <th>User Total Shares</th>
                     <td><?php echo $finaltotal1; ?></td>
                   </tr>
                   <tr>
                     <th>Reward Points</th>
                     <td><?php echo $totalpoints1; ?></td>
                   </tr>
				   <tr>
                     <th>Discount</th>
                     <td><?php echo $discount_value1; ?>%</td>
                   </tr>
				   <tr>
                     <th>Total</th>
                     <td>£<?php echo $newfinal_price ; ?></td>
                   </tr>
                 </tbody>
               </table>	
               <?php }

	              ?> 			
              
                            <form action="order_query.php" method="post" id="send">
                            <input type="hidden" value="<?php echo $username; ?>" name="username" id="username" />
                            <input type="hidden" value="<?php echo $quatity; ?>" name="quantity" id="quantity" />
                            <input type="hidden" value="<?php echo $totalpoints; ?>" name="points" id="points" />
                            <input type="hidden" value="<?php echo $percent; ?>" name="percent" id="percent" />
                            <input type="hidden" value="<?php echo $discount_value; ?>" name="discount_p" id="discount_p" />
                            <input type="hidden" value="<?php echo $new_price; ?>" name="total" id="total" />
							
							 <input type="hidden" value="<?php echo $finaltotal1; ?>" name="totalshares" id="totalshares" />
							 <input type="hidden" value="<?php echo $totalpoints1; ?>" name="socialpoints" id="socialpoints" />
							 <input type="hidden" value="<?php echo $discount_value1; ?>" name="newdiscount" id="newdiscount" />
                            <input type="hidden" value="<?php echo $newfinal_price ; ?>" name="finaltotal" id="finaltotal" />
							
							<input type="submit" value="Proceed To Check out" class="aa-cart-view-btn" />
							</form>
			   
              
             </div>
           </div>
         </div>
       </div>
     </div>
   </div>
 </section>
 <!-- / Cart view section -->


  <!-- Subscribe section -->
  <?php include 'include/subscribe.php';?> 
  <!-- / Subscribe section -->
  
  <!--   footer -->
  <?php include 'include/footer.php';?> 
  <!-- / footer -->

  <!-- Login Modal --> 
  <?php include 'include/loginmodal.php';?> 
  <!-- /Login Modal -->
    

  <!-- jQuery library -->
  <?php include 'include/jquery.php';?> 
  <!-- /jQuery library -->

  </body>
</html>