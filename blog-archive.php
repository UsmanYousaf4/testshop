<?php
session_start();
include 'include/config.php';
?>

<!DOCTYPE html>
<html lang="en">
  
  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->
  
  <body>
  
   <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
 <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Main header section -->
  <?php include 'include/mainheader.php';?> 
  <!-- / Main header section -->
 
  <!-- / Nav -->
   <?php include 'include/nav.php';?> 
  <!-- / Nav --> 
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Blog Archive</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>         
          <li class="active">Blog Archive</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

  <!-- Blog Archive -->
  <section id="aa-blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-blog-archive-area aa-blog-archive-2">
            <div class="row">
              <div class="col-md-9">
                <div class="aa-blog-content">
                  <div class="row">
				  
				  
				  
				  
				  <?php   
	           $results = $mysqli->query("SELECT * FROM pages LIMIT 0, 08");
                 if ($results) { 
               //fetch results set as object and output HTML
                 while($obj = $results->fetch_object())
                  {
			     echo ' 
				  
                    <div class="col-md-4 col-sm-4">					
                      <article class="aa-latest-blog-single">
                        <figure class="aa-blog-img">                    
                          <a href="blog-detail.php?id='.$obj->id.'"><img alt="img" src="img/promo-banner-1.jpg"></a>  
                            <figcaption class="aa-blog-img-caption">
                            <span href="#"><i class="fa fa-eye"></i>5K</span>
                            <a href="#"><i class="fa fa-thumbs-o-up"></i>426</a>
                            <a href="#"><i class="fa fa-comment-o"></i>20</a>
                            <span href="#"><i class="fa fa-clock-o"></i>June 26, 2016</span>
                          </figcaption>                          
                        </figure>
                        <div class="aa-blog-info">
                          <h3 class="aa-blog-title"><a href="#">'.$obj->title.'</a></h3>
                          <p>'.$obj->des.'</p> 
						  
                          <a class="aa-read-mor-btn" href="blog-detail.php?id='.$obj->id.'">Read more <span class="fa fa-long-arrow-right"></span></a>
                        </div>
                      </article>
                    </div>
					';
                       }
                      }
                    ?>  
                                       
                  </div>
                </div>
                <!-- Blog Pagination -->
                <div class="aa-blog-archive-pagination">
                  <nav>
                    <ul class="pagination">
                      <li>
                        <a aria-label="Previous" href="#">
                          <span aria-hidden="true">«</span>
                        </a>
                      </li>
                      <li class="active"><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li>
                        <a aria-label="Next" href="#">
                          <span aria-hidden="true">»</span>
                        </a>
                      </li>
                    </ul>
                  </nav>
                </div>
              </div>
              <div class="col-md-3">
                <aside class="aa-blog-sidebar">
                  
                  <div class="aa-sidebar-widget">
                    <h3>Tags</h3>
                    <div class="tag-cloud">
                      <a href="#">Fashion</a>
                      <a href="#">Ecommerce</a>
                      <a href="#">Shop</a>
                      <a href="#">Hand Bag</a>
                      <a href="#">Laptop</a>
                      <a href="#">Head Phone</a>
                      <a href="#">Pen Drive</a>
                    </div>
                  </div>
                  <div class="aa-sidebar-widget">
                    <h3>Recent Post</h3>
                    <div class="aa-recently-views">
                      <ul>
                        <li>
                          <a class="aa-cartbox-img" href="#"><img src="img/woman-small-2.jpg" alt="img"></a>
                          <div class="aa-cartbox-info">
                            <h4><a href="#">Lorem ipsum dolor sit amet.</a></h4>
                            <p>March 26th 2016</p>
                          </div>                    
                        </li>
                                                            
                      </ul>
                    </div>                            
                  </div>
                </aside>
              </div>
            </div>
           
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Blog Archive -->

<!-- Subscribe section -->
  <?php include 'include/subscribe.php';?> 
  <!-- / Subscribe section -->
  
  <!--   footer -->
  <?php include 'include/footer.php';?> 
  <!-- / footer -->

  <!-- Login Modal --> 
  <?php include 'include/loginmodal.php';?> 
  <!-- /Login Modal -->
    

  <!-- jQuery library -->
  <?php include 'include/jquery.php';?> 
  <!-- /jQuery library -->


  </body>
</html>