<?php
session_start();
include 'include/config.php';


if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{ 
$id=$_POST['id'];
$sql = $mysqli->query("INSERT INTO total_shares (user, shares, points) VALUES ('$_POST[user]','$_POST[share]','$_POST[points]')");
header("Location: blog-detail.php?id=$id");
}
?>

<!DOCTYPE html>
<html lang="en">
  
  <body>
  
   <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->
  
   <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
 <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Main header section -->
  <?php include 'include/mainheader.php';?> 
  <!-- / Main header section -->
 
  <!-- / Nav -->
   <?php include 'include/nav.php';?> 
  <!-- / Nav --> 
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>Blog Details</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>         
          <li class="active">Blog Details</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

  <!-- Blog Archive -->
  <section id="aa-blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-blog-archive-area">
            <div class="row">
              <div class="col-md-9">
			  
                <!-- Blog details -->
                <div class="aa-blog-content aa-blog-details">
				
				<?php
	            
				$id=$_GET['id'];
    
	            $results = $mysqli->query("SELECT * FROM pages where id='$id'");
                if ($results) { 
	
                //fetch results set as object and output HTML
                while($obj = $results->fetch_object())
                {
			    ?>
				
                  <article class="aa-blog-content-single">  
				  
                    <h2><a href="#"><?php echo $obj->title; ?></a></h2>
					
                     <div class="aa-article-bottom">
                      <div class="aa-post-author">
                        Posted By <a href="#">Jackson</a>
                      </div>
                      <div class="aa-post-date">
                        March 26th 2016
                      </div>
                     </div>
					
                    <figure class="aa-blog-img">
                      <a href="#"><img src="img/fashion/3.jpg" alt="fashion img"></a>
                    </figure>
					
                    <p><?php echo $obj->content;  ?></p>
					
                    <div class="blog-single-bottom">
                      <div class="row">
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <div class="blog-single-tag">
                            <span>Tags:</span>
                            <a href="#">Fashion,</a>
                            <a href="#">Beauty,</a>
                            <a href="#">Lifestyle</a>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          
                           <!-- Go to www.addthis.com/dashboard to customize your tools --> 
						   <form id="form-id" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
						   <input type="hidden" name="user" id="user" value="<?php echo $_SESSION['username']; ?>" />
						   <input type="hidden" name="share" id="share" value="1" />
						   <input type="hidden" name="points" id="points" value="10" />
						   <input type="hidden" name="id" id="id" value="<?php echo $id; ?>" />
						   
						   

						   <div class="addthis_inline_share_toolbox" onclick="document.getElementById('form-id').submit();">
						   </form>
						   </div>
                          
                        </div>
                      </div>
                    </div>
                   
                  </article>
				  
				   <?php   
                    }
    
                    }
	               ?>
                  
                  			  
				  
                  <!-- Blog Comment threats -->
                  <div class="aa-blog-comment-threat">
				  <?php
				  $query = $mysqli->prepare("select * from blog_comments where b_id = '$id'");
                  $query->execute();
                  $query->store_result();

                  $rows = $query->num_rows;

                  echo '<h3>Comments('.$rows.')</h3>';				 


				 ?>

                    <div class="comments">
                      <ul class="commentlist">
                        
						
						<?php                 
                        $result = $mysqli->query("select * from blog_comments where b_id = '$id'");	
						if ($result) { 
	                    //fetch results set as object and output HTML
                        while($obj = $result->fetch_object())
                        {
					 echo '
						
                        <li>
                          <div class="media">
                            <div class="media-left">    
                                <img class="media-object news-img" src="img/testimonial-img-2.jpg" alt="img">      
                            </div>
                            <div class="media-body">
                             <h4 class="author-name">'.$obj->name.'</h4>
                             <span class="comments-date"> '.$obj->date.'</span>
							
                             <p>'.$obj->comment.'</p>
							 
                            
							<a href="" class="aa-browse-btn">Share</a>
							<a href="" class="aa-browse-btn"  data-toggle="modal" data-target="#comment-modal">Reply</a>
							'; ?>
							
							<?php
							if (isset($_SESSION['username'])) { 
   
     if ($obj->name == $_SESSION['username'] ) {
							
                            echo' <a href="comment_delete.php?id='.$obj->id.'&bid='.$obj->b_id.'" class="aa-browse-btn">Delete</a>';
	 }
							}
							?>
							<?php echo'
							 
                            </div>
                          </div>
                        </li>
						';
					    }
                      }   
                     ?>
						
						
						
                      </ul>
                    </div>
                    
                  </div>
				  
				  
                  <!-- blog comments form -->
                  <div id="respond">
                    <h3 class="reply-title">Leave a Comment</h3>
					
                    <form id="commentform" action="comment_query.php" method="post">
                      <p class="comment-notes">
                        Your email address will not be published. Required fields are marked <span class="required">*</span>
                      </p>
                      <p class="comment-form-author">
                        <label for="author">Name <span class="required">*</span></label>
                        <input type="text" id="name" name="name" value="" size="30" required="required">
                      </p>
                      <p class="comment-form-email">
                        <label for="email">Email <span class="required">*</span></label>
                        <input type="email" id="email" name="email" value="" aria-required="true" required="required">
                      </p>
                      <p class="comment-form-comment">
                        <label for="comment">Comment</label>
                        <textarea name="comment" cols="45" rows="8" aria-required="true" required="required"></textarea>
                      </p>
                      <p class="form-allowed-tags">
                        You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes:  <code>&lt;a href="" title=""&gt; &lt;abbr title=""&gt; &lt;acronym title=""&gt; &lt;b&gt; &lt;blockquote cite=""&gt; &lt;cite&gt; &lt;code&gt; &lt;del datetime=""&gt; &lt;em&gt; &lt;i&gt; &lt;q cite=""&gt; &lt;s&gt; &lt;strike&gt; &lt;strong&gt; </code>
                      </p>
					  
					  <input type="hidden" value="<?php echo $id ?>" name="blogid" id="blogid" />
                      <p class="form-submit">
                        <input type="submit" name="submit" class="aa-browse-btn" value="Post Comment">
                      </p>        
                    </form>
					
                  </div>
                </div>
              </div>

              <!-- blog sidebar -->
              <div class="col-md-3">
                <aside class="aa-blog-sidebar">
                  
                  <div class="aa-sidebar-widget">
                    <h3>Tags</h3>
                    <div class="tag-cloud">
                      <a href="#">Fashion</a>
                      <a href="#">Ecommerce</a>
                      <a href="#">Shop</a>
                      <a href="#">Hand Bag</a>
                      <a href="#">Laptop</a>
                      <a href="#">Head Phone</a>
                      <a href="#">Pen Drive</a>
                    </div>
                  </div>
                  <div class="aa-sidebar-widget">
                    <h3>Recent Blogs</h3>
                    <div class="aa-recently-views">
                      <ul>
                        <li>
                          <a class="aa-cartbox-img" href="#"><img src="img/woman-small-2.jpg" alt="img"></a>
                          <div class="aa-cartbox-info">
                            <h4><a href="#">Lorem ipsum dolor.</a></h4>
                            <p>March 26th 2016</p>
                          </div>                    
                        </li>                                      
                      </ul>
                    </div>                            
                  </div>
                </aside>
              </div>
            </div>           
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Blog Archive -->


  <!-- Subscribe section -->
  <?php include 'include/subscribe.php';?> 
  <!-- / Subscribe section -->
  
  <!--   footer -->
  <?php include 'include/footer.php';?> 
  <!-- / footer -->

  <!-- Login Modal --> 
  <?php include 'include/loginmodal.php';?> 
  <!-- /Login Modal -->
    

  <!-- jQuery library -->
  <?php include 'include/jquery.php';?> 
  <!-- /jQuery library -->

<!-- Go to www.addthis.com/dashboard to customize your tools --> 
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51e5ef0e75c55ac9"></script> 
  </body>
</html>