<?php
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
//Include FB config file && User class
include 'include/config.php';
include 'include/pagecounter.php';

?>
<!DOCTYPE html>
<html lang="en">

  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->
  
  <body> 
  
   <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->  
         
  <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->

  <!-- Main header section -->
  <?php include 'include/mainheader.php';?> 
  <!-- / Main header section -->
 
  <!-- / Nav -->
   <?php include 'include/nav.php';?> 
  <!-- / Nav -->
  
  <!-- / Slider -->
 
  <!-- / Slider -->
  
   <!-- Products section -->
  <section id="aa-product">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="aa-product-area">
              <div class="aa-product-inner">
                <!-- start prduct navigation -->
                 <ul class="nav nav-tabs aa-products-tab">
                    <li class="active"><a href="#men" data-toggle="tab">Men</a></li>
                    <li><a href="#women" data-toggle="tab">Women</a></li>
                    <li><a href="#sports" data-toggle="tab">Sports</a></li>
                    <li><a href="#electronics" data-toggle="tab">Electronics</a></li>
                  </ul>
                  <!-- Tab panes -->
                  <div class="tab-content">
                    <!-- Start men product category -->
                    <div class="tab-pane fade in active" id="men">
                      <ul class="aa-product-catg">
                      
                        <!-- start single product item -->
                         <?php   
	           $results = $mysqli->query("SELECT * FROM products WHERE category = 'Men' LIMIT 0, 08");
                 if ($results) { 
               //fetch results set as object and output HTML
                 while($obj = $results->fetch_object())
                  {
			     echo '                      
                        
                        <li>
                          <figure>
                            <a class="aa-product-img" href="products-details.php?id='.$obj->id.'"><img src="img/man/'.$obj->img.'" alt="polo shirt img" height="300" ></a>
							
							<form action="cart_query.php" method="post">
							<input type="hidden" id="id" name="id" value="'.$obj->id.'">
                            <input type="hidden" id="title" name="title" value="'.$obj->title.'">
                            <input type="hidden" id="price" name="price" value="'.$obj->price.'">
                            <input type="hidden" id="points" name="points" value="'.$obj->points.'">
                            <input type="hidden" id="img" name="img" value="'.$obj->img.'"> 
							<input type="hidden" id="quantity" name="quantity" value="1"> 
							<button type="submit" class="aa-add-card-btn" ><span class="fa fa-shopping-cart"></span>Add To Cart</button>
                            </form>
							
                            <figcaption>
                            <h4 class="aa-product-title"><a href="#">'.$obj->title.'</a></h4>
                            <span class="aa-product-price">£'.$obj->price.'</span><span class="aa-product-price"><del>£65.50</del></span>
                            </figcaption>
							
                          </figure>                        
                          <div class="aa-product-hvr-content">						    
						    <a href="https://www.facebook.com/" data-toggle="tooltip" data-placement="top" title="Share on Facebook"><span class="fa fa-facebook"></span></a>
                            <a href="https://twitter.com/" data-toggle="tooltip" data-placement="top" title="Share on Twitter"><span class="fa fa-twitter"></span></a> 
                            <a href="https://plus.google.com/" data-toggle="tooltip" data-placement="top" title="Share on Google+"><span class="fa fa-google-plus"></span></a>
							<a href="https://www.linkedin.com/feed/" data-toggle="tooltip" data-placement="top" title="Share on Linkedin"><span class="fa fa-linkedin"></span></a>							
                            <a href="products-details.php?id='.$obj->id.'"  data-toggle="tooltip" data-placement="top" title="View Details"><span class="fa fa-search"></span></a>                          
                          </div>
                          <!-- product badge -->
                          <span class="aa-badge aa-sale" href="#">SALE!</span>
                        </li>
                        
                        ';
                       }
                      }
                    ?>  
                                           
                        <!-- end single product item -->    
                        </ul>
                      <a class="aa-browse-btn" href="products.php">Browse all Product <span class="fa fa-long-arrow-right"></span></a>
                    </div>                                                         
                    <!-- / men product category -->
                    
                    
                    <!-- start women product category -->
                    <div class="tab-pane fade" id="women">
                      <ul class="aa-product-catg">
                      
                       <!-- start single product item -->
                         <?php   
	           $results = $mysqli->query("SELECT * FROM products WHERE category = 'women' LIMIT 0, 08");
                 if ($results) { 
               //fetch results set as object and output HTML
                 while($obj = $results->fetch_object())
                  {
			     echo '                      
                        
                        <li>
                          <figure>
                            <a class="aa-product-img" href="#"><img src="img/man/'.$obj->img.'" alt="polo shirt img" height="300" ></a>
                            <form action="cart_query.php" method="post">
							<input type="hidden" id="id" name="id" value="'.$obj->id.'">
                            <input type="hidden" id="title" name="title" value="'.$obj->title.'">
                            <input type="hidden" id="price" name="price" value="'.$obj->price.'">
                            <input type="hidden" id="points" name="points" value="'.$obj->points.'">
                            <input type="hidden" id="img" name="img" value="'.$obj->img.'"> 
							<input type="hidden" id="quantity" name="quantity" value="1"> 
							<button type="submit" class="aa-add-card-btn" ><span class="fa fa-shopping-cart"></span>Add To Cart</button>
                            </form>
							
                              <figcaption>
                              <h4 class="aa-product-title"><a href="#">'.$obj->title.'</a></h4>
                              <span class="aa-product-price">£'.$obj->price.'</span><span class="aa-product-price"><del>£65.50</del></span>
                            </figcaption>
                          </figure>                        
                          <div class="aa-product-hvr-content">
						    <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
							<a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
						    <a href="#" data-toggle="tooltip" data-placement="top" title="Share on Facebook"><span class="fa fa-facebook"></span></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Share on Twitter"><span class="fa fa-twitter"></span></a>                            
                            <a href="" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" id="" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                          </div>
                          <!-- product badge -->
                          <span class="aa-badge aa-sale" href="#">SALE!</span>
                        </li>
                        
                        ';
                       }
                      }
                    ?>  
                                           
                        <!-- end single product item --> 
                      </ul>
                      <a class="aa-browse-btn" href="#">Browse all Product <span class="fa fa-long-arrow-right"></span></a>
                    </div>
                    <!-- / women product category -->
                    
                    
                    <!-- start sports product category -->
                    <div class="tab-pane fade" id="sports">
                      <ul class="aa-product-catg">
                      
                        <!-- start single product item -->
                       <?php   
	           $results = $mysqli->query("SELECT * FROM products WHERE category = 'sports' LIMIT 0, 08");
                 if ($results) { 
               //fetch results set as object and output HTML
                 while($obj = $results->fetch_object())
                  {
			     echo '                      
                        
                        <li>
                          <figure>
                            <a class="aa-product-img" href="#"><img src="img/man/'.$obj->img.'" alt="polo shirt img" height="300" ></a>
                            <form action="cart_query.php" method="post">
							<input type="hidden" id="id" name="id" value="'.$obj->id.'">
                            <input type="hidden" id="title" name="title" value="'.$obj->title.'">
                            <input type="hidden" id="price" name="price" value="'.$obj->price.'">
                            <input type="hidden" id="points" name="points" value="'.$obj->points.'">
                            <input type="hidden" id="img" name="img" value="'.$obj->img.'"> 
							<input type="hidden" id="quantity" name="quantity" value="1"> 
							<button type="submit" class="aa-add-card-btn" ><span class="fa fa-shopping-cart"></span>Add To Cart</button>
                            </form>
                              <figcaption>
                              <h4 class="aa-product-title"><a href="#">'.$obj->title.'</a></h4>
                              <span class="aa-product-price">£'.$obj->price.'</span><span class="aa-product-price"><del>£65.50</del></span>
                            </figcaption>
                          </figure>                        
                          <div class="aa-product-hvr-content">
						    <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
							<a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
						    <a href="#" data-toggle="tooltip" data-placement="top" title="Share on Facebook"><span class="fa fa-facebook"></span></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Share on Twitter"><span class="fa fa-twitter"></span></a>                            
                            <a href="" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" id="" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                          </div>
                          <!-- product badge -->
                          <span class="aa-badge aa-sale" href="#">SALE!</span>
                        </li>
                        
                        ';
                       }
                      }
                    ?>  
                         <!-- End single product item -->                    
                      </ul>
                    </div>
                    <!-- / sports product category -->
                    
                    
                    <!-- start electronic product category -->
                    <div class="tab-pane fade" id="electronics">
                       <ul class="aa-product-catg">
                       
                        <!-- start single product item -->
                       <?php   
	           $results = $mysqli->query("SELECT * FROM products WHERE category = 'electronics' LIMIT 0, 08");
                 if ($results) { 
               //fetch results set as object and output HTML
                 while($obj = $results->fetch_object())
                  {
			     echo '                      
                        
                        <li>
                          <figure>
                            <a class="aa-product-img" href="#"><img src="img/man/'.$obj->img.'" alt="polo shirt img" height="300" ></a>
                            <form action="cart_query.php" method="post">
							<input type="hidden" id="id" name="id" value="'.$obj->id.'">
                            <input type="hidden" id="title" name="title" value="'.$obj->title.'">
                            <input type="hidden" id="price" name="price" value="'.$obj->price.'">
                            <input type="hidden" id="points" name="points" value="'.$obj->points.'">
                            <input type="hidden" id="img" name="img" value="'.$obj->img.'"> 
							<input type="hidden" id="quantity" name="quantity" value="1"> 
							<button type="submit" class="aa-add-card-btn" ><span class="fa fa-shopping-cart"></span>Add To Cart</button>
                            </form>
                              <figcaption>
                              <h4 class="aa-product-title"><a href="#">'.$obj->title.'</a></h4>
                              <span class="aa-product-price">£'.$obj->price.'</span><span class="aa-product-price"><del>£65.50</del></span>
                            </figcaption>
                          </figure>                        
                          <div class="aa-product-hvr-content">
						    <a href="#" data-toggle="tooltip" data-placement="top" title="Compare"><span class="fa fa-exchange"></span></a>
							<a href="#" data-toggle="tooltip" data-placement="top" title="Add to Wishlist"><span class="fa fa-heart-o"></span></a>
						    <a href="#" data-toggle="tooltip" data-placement="top" title="Share on Facebook"><span class="fa fa-facebook"></span></a>
                            <a href="#" data-toggle="tooltip" data-placement="top" title="Share on Twitter"><span class="fa fa-twitter"></span></a>                            
                            <a href="" data-toggle2="tooltip" data-placement="top" title="Quick View" data-toggle="modal" id="" data-target="#quick-view-modal"><span class="fa fa-search"></span></a>                          
                          </div>
                          <!-- product badge -->
                          <span class="aa-badge aa-sale" href="#">SALE!</span>
                        </li>
                        
                        ';
                       }
                      }
                    ?>  
					   
                         <!-- End single product item -->          
                      </ul>
                      <a class="aa-browse-btn" href="#">Browse all Product <span class="fa fa-long-arrow-right"></span></a>
                    </div>
                    <!-- / electronic product category -->
                  </div>
                  
                  
                                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Products section -->
  
  
 
  <!-- Support section -->
  <section id="aa-support">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-support-area">
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-truck"></span>
                <h4>FREE SHIPPING</h4>
                <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, nobis.</P>
              </div>
            </div>
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-clock-o"></span>
                <h4>30 DAYS MONEY BACK</h4>
                <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, nobis.</P>
              </div>
            </div>
            <!-- single support -->
            <div class="col-md-4 col-sm-4 col-xs-12">
              <div class="aa-support-single">
                <span class="fa fa-phone"></span>
                <h4>SUPPORT 24/7</h4>
                <P>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam, nobis.</P>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Support section -->
  

  <!-- Client Brand -->
  <section id="aa-client-brand">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-client-brand-area">
            <ul class="aa-client-brand-slider">
              <li><a href="#"><img src="img/client-brand-java.png" alt="java img"></a></li>
              <li><a href="#"><img src="img/client-brand-jquery.png" alt="jquery img"></a></li>
              <li><a href="#"><img src="img/client-brand-html5.png" alt="html5 img"></a></li>
              <li><a href="#"><img src="img/client-brand-css3.png" alt="css3 img"></a></li>
              <li><a href="#"><img src="img/client-brand-wordpress.png" alt="wordPress img"></a></li>
              <li><a href="#"><img src="img/client-brand-joomla.png" alt="joomla img"></a></li>
              <li><a href="#"><img src="img/client-brand-java.png" alt="java img"></a></li>
              <li><a href="#"><img src="img/client-brand-jquery.png" alt="jquery img"></a></li>
              <li><a href="#"><img src="img/client-brand-html5.png" alt="html5 img"></a></li>
              <li><a href="#"><img src="img/client-brand-css3.png" alt="css3 img"></a></li>
              <li><a href="#"><img src="img/client-brand-wordpress.png" alt="wordPress img"></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Client Brand -->

  <!-- Subscribe section -->
  <?php include 'include/subscribe.php';?> 
  <!-- / Subscribe section -->
  
  <!--   footer -->
  <?php include 'include/footer.php';?> 
  <!-- / footer -->

  <!-- Login Modal --> 
  <?php include 'include/loginmodal.php';?> 
  <!-- /Login Modal -->
    

  <!-- jQuery library -->
  <?php include 'include/jquery.php';?> 
  <!-- /jQuery library -->

  </body>
</html>