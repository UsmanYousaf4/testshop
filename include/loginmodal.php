<!-- Login Modal -->  
  <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="height:580px">                      
        <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4>Login or Register</h4>
		  
          <form action="loginquery.php" method="post" class="aa-login-form" action="">
		    <i class="ace-icon fa fa-user"></i>
            <label for="">Username or Email address<span>*</span></label>			
            <input type="text" id="username" name="username" placeholder="Username or email" required />
			<i class="ace-icon fa fa-lock"></i>
			<label for="">Password<span>*</span></label>
            <input type="password" id="password" name="password"  placeholder="Password" required />
            <button class="aa-browse-btn" type="submit">Login</button>
            <label for="rememberme" class="rememberme"><input type="checkbox" id="rememberme"> Remember me </label>
            <p class="aa-lost-password"><a href="#">Lost your password?</a></p>
            <div class="aa-register-now">
              Don't have an account?<a href="register.php">Register now!</a>
            </div><br/>
            <div class="aa-myaccount-register">                 
                 <a href="fbconfig.php" class="btn  btn-social btn-lg btn-facebook">
                 <i class="fa fa-facebook"></i> Sign in with Facebook
                 </a> <br/> 
				   
                 </div>            
          </form>          
        </div>                        
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>  
  


