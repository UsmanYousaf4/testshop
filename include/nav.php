<!-- menu -->
  <section id="menu">
    <div class="container">
      <div class="menu-area">
        <!-- Navbar -->
        <div class="navbar navbar-default" role="navigation">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>          
          </div>
          <div class="navbar-collapse collapse">
            <!-- Left nav -->
            <ul class="nav navbar-nav">
              <li><a href="index.php">Home</a></li>
              <li><a href="products.php?var=men">Men</a> </li>
			  <li><a href="products.php?var=women">Women</a> </li>
			  <li><a href="products.php?var=kids">Kids</a> </li>
                           
              <li><a href="products.php?var=Sports">Sports</a></li>
             <li><a href="products.php?var=Electronics">Electronics</a>
                   </li>
              <li><a href="products.php?var=Furniture">Furniture</a></li>            
              <li><a href="blog-archive.php">Blog</a></li>
			  <li><a href="about.php">About</a></li>
              <li><a href="contact.php">Contact</a></li>
              
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>       
    </div>
  </section>
  <!-- / menu -->