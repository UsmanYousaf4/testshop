 <!-- Start header section -->
  <header id="aa-header">
    <!-- start header top  -->
    <div class="aa-header-top">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="aa-header-top-area">
              <!-- start header top left -->
              <div class="aa-header-top-left">
                <!-- start language -->
                <div class="aa-language">
                  <div class="dropdown">
                    <a class="btn dropdown-toggle" href="#" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <img src="img/flag/english.jpg" alt="english flag">ENGLISH
                      <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li><a href="#"><img src="img/flag/english.jpg" alt="">ENGLISH</a></li>
                    </ul>
                  </div>
                </div>
                <!-- / language -->

                <!-- start currency -->
                <div class="aa-currency">
                  <div class="dropdown">
                    <a class="btn dropdown-toggle" href="#" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                      <i class="fa fa-gbp"></i>GBP
                      <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                      <li><a href="#"><i class="fa fa-gbp"></i>GBP</a></li>                      
                    </ul>
                  </div>
                </div>
                <!-- / currency -->
				
                <!-- start cellphone -->
                <div class="cellphone hidden-xs">
                  <p><span class="fa fa-phone"></span>0044-114-444-444</p>
                </div>
                <!-- / cellphone -->
				
              </div>
              <!-- / header top left -->
              <div class="aa-header-top-right">
                <ul class="aa-head-top-nav-right">
                  <li>
                  <?php 
                  if (isset($_SESSION['username'])) {   
                  echo "Welcome:<b style='color:#3CF;'> ".$_SESSION['username']."</b>|<a href='account.php'>My Account</a>";
                  }   
                  else {
                  echo " <a href='account.php'>My Account</a>"; 
                  }
                  ?>
                  </li>
                 
                  <li class="hidden-xs"><a href="my_cart.php">My Cart</a></li>
                  <li class="hidden-xs"><a href="billing_details.php">Checkout</a></li>
                  <li>
				  <?php 
                  if (isset($_SESSION['username'])) {
                  echo "<a href='logout.php'>Logout</a>"; 
                  }
                  else {
                  echo "<a href='' data-toggle='modal' data-target='#login-modal'>Login</a>"; 
                   }
                  ?>                 
                                  
                 </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header top  -->

    <!-- start header bottom  -->
    <div class="aa-header-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="aa-header-bottom-area">
              <!-- logo  -->
              <div class="aa-logo">
                <!-- Text based logo -->
                <a href="index.php">
                  <span class="fa fa-shopping-cart"></span>
                  <p>Test<strong>Shop</strong> <span>Your Shopping Partner</span></p>
                </a>
                <!-- img based logo -->
                <!-- <a href="index.php"><img src="img/logo.jpg" alt="logo img"></a> -->
              </div>
              <!-- / logo  -->
			  
			  
               <!-- cart box -->
			   <div class="aa-cartbox">
			   <?php 
                  if (isset($_SESSION['username'])) {
					  $con=mysqli_connect("localhost","ubuntu","421986_usman","shop_db");
        if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
					  $username=  $_SESSION['username'];					  
					  $results1 = mysqli_query($con,"SELECT sum(quantity) FROM usercart WHERE `username` = '$username'");
		              while ($rows = mysqli_fetch_array($results1)) {
			          $quatity = $rows['sum(quantity)'];
					  
					  echo '<a class="aa-cart-link" href="my_cart.php">
                  <span class="fa fa-shopping-basket"></span>
                  <span class="aa-cart-title">SHOPPING CART</span>
                  <span class="aa-cart-notify">'.$quatity.'</span>
                </a>';
					  }
				?>
				<div class="aa-cartbox-summary">
                  <ul>
					<?php
                        $username=  $_SESSION['username'];
                        $results = $mysqli->query("select * from usercart where username= '$username' ORDER BY id desc ");	
		        if ($results) {	
                        //fetch results set as object and output HTML
                    while($obj = $results->fetch_object())
                        {
			            $points = $obj->points;						
					  
					  echo'<li>
                      <a class="aa-cartbox-img" href="#"><img src="img/man/'.$obj->img.'" alt="img"></a>
                      <div class="aa-cartbox-info">
                        <h4><a href="#">'.$obj->ptitle.'</a></h4>
                        <p>1 x $250</p>
                      </div>
                      <a class="aa-remove-product" href="#"><span class="fa fa-times"></span></a>
                    </li>'; 
                    }
				}
				  
				?>
				</ul>
                  <a class="aa-cartbox-checkout aa-primary-btn" href="my_cart.php">Open Cart</a>
                </div>
				
			    <?php
				  }
                  else {
                  echo '<a class="aa-cart-link" href="#">
                  <span class="fa fa-shopping-basket"></span>
                  <span class="aa-cart-title">SHOPPING CART</span>
                  <span class="aa-cart-notify">0</span>
                </a>'; 
                   }
                  ?> 
			   	</div>
              <!-- / cart box -->
			  
			  
			  
			  
              <!-- search box -->
              <div class="aa-search-box">
                <form action="search.php" method="post">
                  <input type="text" name="search" id="search" placeholder="Search here ex. 'man' ">
                  <button type="submit"><span class="fa fa-search"></span></button>
                </form>
              </div>
              <!-- / search box -->     

			  
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- / header bottom  -->
  </header>
  <!-- / header section -->