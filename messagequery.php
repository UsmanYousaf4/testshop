<?php
// Create connection
include 'include/config.php';
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
date_default_timezone_set('UTC');

$date= date("m.d.y");
$time= date('H:i:s');
$day= date('l');

$sql="INSERT INTO messages (name, email, subject, message, company, date, time, day)
VALUES
('$_POST[name]','$_POST[email]','$_POST[subject]','$_POST[message]','$_POST[company]','$date','$time','$day')";

if (mysqli_query($conn, $sql)) {
    $msg = "Your message was successfully sent to the Test Shop Support Team. Thank you for taking the time to contact us. ";   
    header("Location: contact.php?msg=$msg"); 
    exit;
} else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
}
//}
mysqli_close($conn);
?>
