<?php
session_start();

include 'include/config.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') 
{ 
$id=$_POST['id'];
$sql = $mysqli->query("INSERT INTO total_shares (user, shares, points) VALUES ('$_POST[user]','$_POST[share]','$_POST[points]')");
header("Location: products-details.php?id=$id");
}
?>

<!DOCTYPE html>
<html lang="en">
  <!--  header section -->
  <?php include 'include/header.php';?>  
  <!-- /header section -->
  
  <body>  
  
    <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
    <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
    <!-- END SCROLL TOP BUTTON -->

  <!-- Main header section -->
  <?php include 'include/mainheader.php';?> 
  <!-- / Main header section -->
 
  <!-- / Nav -->
   <?php include 'include/nav.php';?> 
  <!-- / Nav -->
  
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>T-Shirt</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>         
          <li><a href="#">Product</a></li>
          <li class="active">T-shirt</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

  <!-- product category -->
  <section id="aa-product-details">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-product-details-area">
            <div class="aa-product-details-content">
              <div class="row">
                
				<?php
				$id=$_GET['id'];
				$results = $conn->query("SELECT * FROM products WHERE id='$id'");
				if ($results) { 
				
					//fetch results set as object and output HTML
					while($obj = $results->fetch_object())
					{
			     ?>
			
				<!-- Modal view slider -->
                <div class="col-md-5 col-sm-5 col-xs-12">                              
                  <div class="aa-product-view-slider">                                
                    <div id="demo-1" class="simpleLens-gallery-container">
                      <div class="simpleLens-container">
                        <div class="simpleLens-big-image-container">
						<a data-lens-image="img/man/<?php echo $obj->img; ?>" class="simpleLens-lens-image"><img src="img/man/<?php echo $obj->img; ?>" class="simpleLens-big-image"></a></div>
                      </div>
                      </div>
                  </div>
                </div>
                <!-- Modal view content -->						
				
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class="aa-product-view-content">
				  
                    <h3><?php echo $obj->title; ?></h3>
                    <div class="aa-price-block">
                      <span class="aa-product-view-price">Price: £<?php echo $obj->price; ?></span>
                      <p class="aa-product-avilability">Avilability: <span><?php echo $obj->availability; ?></span></p>
                    </div>
					
                    <p><?php echo $obj->des; ?></p>
					
                <form id="addtocart" action="cart_query.php" method="post" >                   

				   <h4>Size</h4>
                    <div class="aa-prod-view-size">
                      <a href="#">S</a>
                      <a href="#">M</a>
                      <a href="#">L</a>
                      <a href="#">XL</a>
                    </div>
					
                    <h4>Color</h4>
                    <div class="aa-color-tag">
                      <a href="#" class="aa-color-green"></a>
                      <a href="#" class="aa-color-yellow"></a>
                      <a href="#" class="aa-color-pink"></a>                      
                      <a href="#" class="aa-color-black"></a>
                      <a href="#" class="aa-color-white"></a>                      
                    </div>
					
					
                    <div class="aa-prod-quantity">
					Quantity:
                      
                        <select id="quantity" name="quantity">
                          <option selected="1" value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                        </select>
                     
					  <p class="aa-prod-category">
                        Category: <a href="#">Polo T-Shirt</a>
                      </p>
                    </div>
								  
					<input type="hidden" id="id" name="id" value="<?php echo $obj->id; ?>">
                    <input type="hidden" id="title" name="title" value="<?php echo $obj->title; ?>">
                    <input type="hidden" id="price" name="price" value="<?php echo $obj->price; ?>">
                    <input type="hidden" id="points" name="points" value="<?php echo $obj->points; ?>">
                    <input type="hidden" id="img" name="img" value="<?php echo $obj->img; ?>">                          
                    
                    <div class="aa-prod-view-bottom">
					<button type="submit" class="aa-add-to-cart-btn">Add To Cart</button>
					</div>
				</form>
                  </div>				  
				<?php
                       }
                      }
                    ?>  
				
					<div class="aa-product-hvr-content">
					<!-- Go to www.addthis.com/dashboard to customize your tools --> 
						   <form id="form-id" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
						   <input type="hidden" name="user" id="user" value="<?php echo $_SESSION['username']; ?>" />
						   <input type="hidden" name="share" id="share" value="1" />
						   <input type="hidden" name="points" id="points" value="10" />
						   <input type="hidden" name="id" id="id" value="<?php echo $id; ?>" />				   
						   <div class="addthis_inline_share_toolbox" onclick="document.getElementById('form-id').submit();">
						   </form>
						   </div>
					</div>
					
				

				
              </div>
            </div>
            <div class="aa-product-details-bottom">
              <ul class="nav nav-tabs" id="myTab2">
                <li><a href="#description" data-toggle="tab">Description</a></li>
                <li><a href="#review" data-toggle="tab">Reviews</a></li>                
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane fade in active" id="description">
                  Prodcut description 
				</div>
				
				
                <div class="tab-pane fade " id="review">
                 <div class="aa-product-review-area">
							 
                   <h4>Product Reviews</h4> 
                   <ul class="aa-review-nav">
                     <?php                 
                        $result = $mysqli->query("select * from p_reviews where p_id = '$id'");	
						if ($result) { 
	                    //fetch results set as object and output HTML
                        while($obj = $result->fetch_object())
                        {
					 echo '
                      <li>
                        <div class="media">
                          <div class="media-left">
                            <a href="#">
                              <img class="media-object" src="img/testimonial-img-3.jpg" alt="girl image">
                            </a>
                          </div>
                          <div class="media-body">
                            <h4 class="media-heading"><strong>'.$obj->name.'</strong></h4>
                            <div class="aa-product-rating">
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star"></span>
                              <span class="fa fa-star-o"></span>
                            </div>
                            <p>'.$obj->review.'</p>
							Day: <span>'.$obj->day.'</span> - Time: <span>'.$obj->time.'</span> - Date: <span>'.$obj->date.' 26, 2016</span>
							<p>
							<a href="" class="aa-browse-btn">Share</a>
							<a href="" class="aa-browse-btn"  data-toggle="modal" data-target="#comment-modal">Reply</a> ';
							?>
							<?php
									
							if (isset($_SESSION['username'])) { 
   
     if ($obj->name == $_SESSION['username'] ) {
							
                            echo'<a href="review_delete.php?id='.$obj->id.'&pid='.$obj->p_id.'" class="aa-browse-btn">Delete </a></p>';
	 }
							}
							?>
							
							
                           <?php echo'
                                       
							
                          </div>
                        </div>
                      </li>';
					    }
                      }   
                     ?>
					  
                   </ul>
                   <h4>Add a review</h4>
                   <div class="aa-your-rating">
                     <p>Your Rating</p>
                     <a href="#"><span class="fa fa-star-o"></span></a>
                     <a href="#"><span class="fa fa-star-o"></span></a>
                     <a href="#"><span class="fa fa-star-o"></span></a>
                     <a href="#"><span class="fa fa-star-o"></span></a>
                     <a href="#"><span class="fa fa-star-o"></span></a>
                   </div>
				   
                   <!-- review form -->
                   <form class="aa-review-form" action="review_query.php" method="post">
				   
                      <div class="form-group">
                        <label for="message">Your Review</label>
                        <textarea class="form-control" rows="3" name="review"></textarea>
                      </div>
					  
                      <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" required />
                      </div>  
					  
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email"  placeholder="example@gmail.com" required/>
                      </div>
					  
                      <input type="hidden" value="<?php echo $id ?>" name="productid" id="productid" />
                      <button type="submit" class="btn btn-default aa-review-submit">Submit</button>
                   </form>
				   
				   
                 </div>
                </div>            
              </div>
            </div>
			
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / product category -->


<!-- Subscribe section -->
  <?php include 'include/subscribe.php';?> 
  <!-- / Subscribe section -->
  
  <!--   footer -->
  <?php include 'include/footer.php';?> 
  <!-- / footer -->

  <!-- Login Modal --> 
  <?php include 'include/loginmodal.php';?> 
  <!-- /Login Modal -->
    

  <!-- jQuery library -->
  <?php include 'include/jquery.php';?> 
  <!-- /jQuery library -->

  <!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer></script>


  
  </body>
</html>