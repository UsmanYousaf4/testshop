<?php
session_start();
include 'include/config.php';
?>

<!DOCTYPE html>
<html lang="en">
  
  <!--  header section -->
  <?php include 'include/header.php';?>    
  <!-- /header section -->
  
  <body>
   
   <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
 <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->


  <!-- Main header section -->
  <?php include 'include/mainheader.php';?> 
  <!-- / Main header section -->
 
  <!-- / Nav -->
   <?php include 'include/nav.php';?> 
  <!-- / Nav --> 
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>About Us</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>         
          <li class="active">About Us</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

  <!-- Blog Archive -->
  <section id="aa-blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-blog-archive-area">
            <div class="row">
              <div class="col-md-9">
			  
			  
                <!-- Blog details -->
                <div class="aa-blog-content aa-blog-details">
				
				<?php
	
    
	            $results = $mysqli->query("SELECT * FROM pages where title='aboutus'");
                if ($results) { 
	
                //fetch results set as object and output HTML
                while($obj = $results->fetch_object())
                {
			    ?>
                  <article class="aa-blog-content-single">                        
                    <h2><a href="#">About Us</a></h2>
                     
                    
                    <p><?php echo $obj->content;  ?> </p>
					
					
                    <div class="blog-single-bottom">
                      <div class="row">
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <div class="blog-single-tag">
                            <span>Tags:</span>
                            <a href="#">Fashion,</a>
                            <a href="#">Beauty,</a>
                            <a href="#">Lifestyle</a>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <div class="blog-single-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>
                   
                  </article>
                   <?php   
                    }
    
                    }
	               ?>
                  
                  
                </div>
              </div>

              <!-- blog sidebar -->
              <div class="col-md-3">
                <aside class="aa-blog-sidebar">
                  
                  <div class="aa-sidebar-widget">
                    <h3>Tags</h3>
                    <div class="tag-cloud">
                      <a href="#">Fashion</a>
                      <a href="#">Ecommerce</a>
                      <a href="#">Shop</a>
                      <a href="#">Hand Bag</a>
                      <a href="#">Laptop</a>
                      <a href="#">Head Phone</a>
                      <a href="#">Pen Drive</a>
                    </div>
                  </div>
                  <div class="aa-sidebar-widget">
                    <h3>Recent Blogs</h3>
                    <div class="aa-recently-views">
                      <ul>
                        <li>
                           <div class="aa-cartbox-info">
                            <h4><a href="#">Lorem ipsum dolor sit amet.</a></h4>
                            <p>March 26th 2016</p>
                          </div>                    
                        </li>
                                                             
                      </ul>
                    </div>                            
                  </div>
                </aside>
              </div>
            </div>           
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Blog Archive -->

<!-- Subscribe section -->
  <?php include 'include/subscribe.php';?> 
  <!-- / Subscribe section -->
  
  <!--   footer -->
  <?php include 'include/footer.php';?> 
  <!-- / footer -->

  <!-- Login Modal --> 
  <?php include 'include/loginmodal.php';?> 
  <!-- /Login Modal -->
    

  <!-- jQuery library -->
  <?php include 'include/jquery.php';?> 
  <!-- /jQuery library -->


  </body>
</html>