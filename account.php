<?php
session_start();
$_SESSION['url'] = $_SERVER['REQUEST_URI'];
include 'include/config.php';
include 'include/sessionchecker.php';
?>

<!DOCTYPE html>
<html lang="en">
  <!--  header section -->
  <?php include 'include/header.php';?>  
  <!-- /header section -->
  
  <body>  
  <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8&appId=1761410717508763";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!-- wpf loader Two -->
    <div id="wpf-loader-two">          
      <div class="wpf-loader-two-inner">
        <span>Loading</span>
      </div>
    </div> 
    <!-- / wpf loader Two -->       
    <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-chevron-up"></i></a>
    <!-- END SCROLL TOP BUTTON -->

  <!-- Main header section -->
  <?php include 'include/mainheader.php';?> 
  <!-- / Main header section -->
 
  <!-- / Nav -->
   <?php include 'include/nav.php';?> 
  <!-- / Nav -->
  
 
  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="img/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2>My Account</h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>         
          <li class="active">My Account</li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

  <!-- product category -->
  <section id="aa-product-details">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-product-details-area">
            <div class="aa-product-details-content">
              <div class="row">
                <span>
   <?php 
   if(isset($_GET['msg1']))
  echo "<p class='alert alert-success'> ".$_GET['msg1']." </p>";
  ?>
  </span>
				
				<!-- Modal view slider -->
                <div class="col-md-5 col-sm-5 col-xs-12">                              
                  <div class="aa-product-view-slider">                                
                    <div id="demo-1" class="simpleLens-gallery-container">
                      <div class="simpleLens-container">
                        <div class="simpleLens-big-image-container">
						<a data-lens-image="img/profile-pic.jpg" class="simpleLens-lens-image"><img src="img/profile-pic.jpg" class="simpleLens-big-image"></a>
						</div>
					  </div>
					</div>
				</div>
				
				<?php
				
		$result2 = mysqli_query($conn,"SELECT sum(shares),sum(points) FROM total_shares WHERE `user` = '$username'");		
		
		while ($rows = mysqli_fetch_array($result2)) {
			
			
			$finaltotal= $rows['sum(shares)'];
			$totalpoints= $rows['sum(points)'];
			
			if ($totalpoints <=  "100") {
			  $discount_value = '10';
			 
            
            } else if ($points <=  "200") {
			  $discount_value = '20';
			  
          	}
			else {
             $discount_value = '0';
            }
			
            ?>
			
			<div class="aa-sidebar-widget">
              <h3>Category</h3>
              <ul class="aa-catg-nav">
                <table class="aa-totals-table">
                 <tbody>
                   <tr>
                     <th>Total Social Media Shares</th>
                     <td><?php echo $finaltotal; ?></td>
                   </tr>
                   <tr>
                     <th>Total Share Reward Points</th>
                     <td><?php echo $totalpoints; ?></td>
                   </tr>
				   <tr>
                     <th>Discount</th>
                     <td><?php echo $discount_value; ?>%</td>
                   </tr>
                 </tbody>
               </table>
              </ul>
            </div>			
           </div>
				<?php 
				}

	              ?> 
				
				
				<?php
 
                        $username =  $_SESSION['username'];                      
                        $results = $mysqli->query("select * from users where username = '$username' ");	
						if ($results) { 
	
                        //fetch results set as object and output HTML
                        while($obj = $results->fetch_object())
                        {
				 echo '
				
				
                <!-- Modal view content -->						
				
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class="aa-product-view-content">
				  
                    <form class="" method="post" action="account_update.php">
						
						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Firstname </label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="Firstname" id="Firstname"  value="'.$obj->firstname.'" required />
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label for="name" class="cols-sm-2 control-label">Lastname</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="Lastname" id="Lastname"  value="'.$obj->lastname.'" required />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="email" class="cols-sm-2 control-label">Your Email</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="email" id="email"  value="'.$obj->email.'" required />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="username" class="cols-sm-2 control-label">Username</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="username" id="username"  value="'.$obj->username.'" required />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="password" class="cols-sm-2 control-label">Password</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
									<input type="password" class="form-control" name="password" id="password"  value="'.$obj->password.'" required />
								</div>
							</div>
						</div>

						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Address </label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-book fa-lg" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="address" id="address"  value="'.$obj->address.'" required />
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Mobile</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-phone fa-lg" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="mobile" id="mobile"  value="'.$obj->mobile.'" required />
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Facebook Link</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="facebook" id="facebook"  value="'.$obj->facebook.'" required />
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<label for="confirm" class="cols-sm-2 control-label">Twitter Link</label>
							<div class="cols-sm-10">
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></span>
									<input type="text" class="form-control" name="twitter" id="twitter"  value="'.$obj->twitter.'"/>
								</div>
							</div>
						</div>

						<div class="form-group ">
						
						<input type="hidden" name="sessionuser" id="sessionuser" value="'.$username.'">
						<button type="submit" class="btn btn-primary btn-lg btn-block login-button">Update</button>
							
						</div>
						
					</form>
                  </div>				  
				';
                       }
                      }
                    ?>  
				
					
					</div>
				
				
              </div>
            </div>
            <div class="aa-product-details-bottom">
              <ul class="nav nav-tabs" id="myTab2">
                <li><a href="#description" data-toggle="tab">Shopping Cart</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div class="tab-pane fade in active" id="description">
                <div class="cart-view-table">
             <form action="">
			 <?php 
             if(isset($_GET['msg']))
             echo "<p class='alert alert-success'> ".$_GET['msg']." </p>";
             ?>
               <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th></th>
                        <th></th>
                        <th>Product</th>
                        <th>Price</th>
						<th>Reward Points</th>
                        <th>Quantity</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody>
					
					<?php

                        $username=  $_SESSION['username'];
                        $results = $mysqli->query("select * from usercart where username= '$username' ORDER BY id desc ");	
						if ($results) { 
	
                        //fetch results set as object and output HTML
                        while($obj = $results->fetch_object())
                        {
			            $points = $obj->points;
					  echo '
					
                      <tr>
                        <td><a class="remove" href="product_delete.php?id='.$obj->id.'"><fa class="fa fa-close"></fa></a></td>
                        <td><a href="#"><img src="img/man/'.$obj->img.'" alt="img"></a></td>
                        <td><a class="aa-cart-title" href="#">'.$obj->ptitle.'</a></td>
                        <td>£'.$obj->price.'</td>
						<td>'.$obj->points.'</td>
                        <td><input class="aa-cart-quantity" type="number" value="1"></td>
                        <td>£'.$obj->totalcart.'</td>
                      </tr>                                          
					  
					  ';
					  }
                    }
	               ?>
				   
                      </tbody>
                  </table>
                </div>
             </form>
			 
			 <?php		
		
		$result1 = mysqli_query($conn,"SELECT sum(totalcart),sum(points),sum(quantity) FROM usercart WHERE `username` = '$username'");
		
		
		while ($rows = mysqli_fetch_array($result1)) {
			
			$quatity = $rows['sum(quantity)'];
			$finaltotal= $rows['sum(totalcart)'];
			$totalpoints= $rows['sum(points)'];
			
			if ($totalpoints <=  "100") {
			  $percent = '10';
			 $discount_value = ($finaltotal / 100) * $percent;
            
            } else if ($points <=  "200") {
			  $percent = '20';
			  $discount_value = ($finaltotal / 100) * $percent;
          
			}
			else {
             $discount_value = '0';
            }
			
			$new_price = $finaltotal - $discount_value;
            ?>
             <!-- Cart Total view -->
             <div class="cart-view-total">
               <h4>Cart Totals</h4>
               <table class="aa-totals-table">
                 <tbody>
                   <tr>
                     <th>Subtotal</th>
                     <td>£<?php echo $finaltotal; ?></td>
                   </tr>
                   <tr>
                     <th>Reward Points</th>
                     <td><?php echo $totalpoints ; ?></td>
                   </tr>
				   <tr>
                     <th>Discount price</th>
                     <td>£<?php echo $discount_value; ?></td>
                   </tr>
				   <tr>
                     <th>Total</th>
                     <td>£<?php echo $new_price; ?></td>
                   </tr>
                 </tbody>
               </table>
			   
			    <?php }

	              ?>             
                            <form action="order_query.php" method="post" id="send">
                            <input type="hidden" value="<?php echo $username; ?>" name="username" id="username" />
                            <input type="hidden" value="<?php echo $quatity; ?>" name="quantity" id="quantity" />
                            <input type="hidden" value="<?php echo $totalpoints; ?>" name="points" id="points" />
                            <input type="hidden" value="<?php echo $percent; ?>" name="percent" id="percent" />
                            <input type="hidden" value="<?php echo $discount_value; ?>" name="discount_p" id="discount_p" />
                            <input type="hidden" value="<?php echo $new_price; ?>" name="total" id="total" />
							
							<input type="submit" value="Proceed To Check out" class="aa-cart-view-btn" />
							<input type="button" onclick="window.location.href='index.php'" value="Continue Shopping" class="aa-cart-view-btn" />
							</form>
			   
              
             </div>
           </div>
				</div>
				
				
                            
              </div>
            </div>
			
            
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / product category -->


<!-- Subscribe section -->
  <?php include 'include/subscribe.php';?> 
  <!-- / Subscribe section -->
  
  <!--   footer -->
  <?php include 'include/footer.php';?> 
  <!-- / footer -->

  <!-- Login Modal --> 
  <?php include 'include/loginmodal.php';?> 
  <!-- /Login Modal -->
    

  <!-- jQuery library -->
  <?php include 'include/jquery.php';?> 
  <!-- /jQuery library -->

  <!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer></script>


  
  </body>
</html>